/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.uni_leipzig.informatik.asv.corpusUtils.corpus.util;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.net.URISyntaxException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import de.uni_leipzig.informatik.asv.corpusUtils.helper.utils.FileUtils;

@RunWith(JUnit4.class)
public class ContentCleanerTest {
	
	
	
	@Test
	public void testCleanContent() {
		try {
			String htmlSource = ContentCleanerTest.class.getResource("html_source").toURI().getPath();
			String source = FileUtils.readLineFromFile(htmlSource);
			
			String plainText = ContentCleanerTest.class.getResource("plain_text").toURI().getPath();
			String target = FileUtils.readLineFromFile(plainText);
			String clean = ContentCleaner.cleanContent(source);

			assertEquals(target, clean);
			
		} catch (URISyntaxException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	
	}
	
	@Test
	public void testCleanWhiteSpace() {
		try {
			String plainText = ContentCleanerTest.class.getResource("plain_text").toURI().getPath();
			String target = FileUtils.readLineFromFile(plainText);
			
			String noisyTarget = target.replaceAll("\\s", "    ");
			noisyTarget = ContentCleaner.cleanWhiteSpaces(noisyTarget);
			assertEquals(target, noisyTarget);
			
		} catch (URISyntaxException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
	
	@Test
	public void testRemovePunctuation() {
		try {
			String plainText = ContentCleanerTest.class.getResource("plain_text").toURI().getPath();
			String source = FileUtils.readLineFromFile(plainText);

			String plainTextNoPunct = ContentCleanerTest.class.getResource("plain_text_nopunct").toURI().getPath();
			String target = FileUtils.readLineFromFile(plainTextNoPunct);
			
			source = ContentCleaner.removePunctuation(source);
			assertEquals(target, source);
			
		} catch (URISyntaxException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	@Test 
	public void testRemovePunctuationAndDashes() {
		try {
			String plainText = ContentCleanerTest.class.getResource("plain_text").toURI().getPath();
			String source = FileUtils.readLineFromFile(plainText);

			String plainTextNoPunct = ContentCleanerTest.class.getResource("plain_text_nopunct_nodash").toURI().getPath();
			String target = FileUtils.readLineFromFile(plainTextNoPunct);
			
			source = ContentCleaner.removePunctuation(source);
			source = ContentCleaner.removeDashes(source);
			assertEquals(target, source);
			
		} catch (URISyntaxException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
}
