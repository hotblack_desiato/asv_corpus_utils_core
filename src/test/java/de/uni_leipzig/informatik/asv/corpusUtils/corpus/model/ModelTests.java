/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.uni_leipzig.informatik.asv.corpusUtils.corpus.model;

import static org.junit.Assert.assertNotNull;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import de.uni_leipzig.informatik.asv.corpusUtils.corpus.exporter.CorpusExporter;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.exporter.FeatureFrequencyCorpusExporter;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.importer.CorpusImporter;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.util.ExportSettings;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.util.ImportSettings;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.utils.FileUtils;

@RunWith(JUnit4.class)

public class ModelTests {
	
	@Test
	public void testReadDictionary() {
		Corpus c = readCorpus("test_corpus.dat","test_vocabulary.dat", false);
		URL u = this.getClass().getResource("test_vocabulary.dat");
		try {
			File f = new File(u.toURI());
			String[] words = FileUtils.readLinesFromFile(f.getCanonicalPath());
			
			for(int w=0;w<c.getDictionary().getNumberOfWordTypes();++w)
				Assert.assertEquals(words[w], c.getDictionary().getWordTypeById(w).getValue());
			
		} catch (IOException e) {
			e.printStackTrace();
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testDeduplicateCorpus() {
		
		
		Corpus c = readCorpus("test_corpus.dat","test_vocabulary.dat", true);
		int numberOfDocumentsOrig = c.getNumberOfDocuments();
		
		
		c.deduplicateCorpus(5,1000,.93);
		
		Assert.assertEquals(numberOfDocumentsOrig - 3, c.getNumberOfDocuments());
			
	}
	
	@Test
	public void testPruneDictionary() {
		Corpus c = readCorpus("prune_corpus", "prune_vocab", false);
		URL u = this.getClass().getResource("prune_vocab");
		try {
//			File f = new File(u.toURI());
//			String[] words = FileUtils.readLinesFromFile(f.getCanonicalPath());
//			
//			TreeMap<Integer, WordType> typeIds = new TreeMap<Integer, WordType>();
//			for(String w : words) {
//				WordType wt = c.getWordType(w, false);
//				assertNotNull(wt);
//				if(wt.getCorpusFrequency() >= 5)
//					typeIds.put(wt.getId(), wt);
//			}
			
			c.pruneDictionaryMinFreq(5);
			String[] words = new String[]{"word2", "word4", "word5", "word6","word7", "word8"};
			int[] freqs = new int[]{6, 10, 6, 7, 8, 9};
			for(int w=0;w<c.getDictionary().getNumberOfWordTypes();++w) {
				WordType wt = c.getDictionary().getWordTypeById(w);
				assertNotNull(wt);
				Assert.assertEquals(words[w], wt.getValue());
				Assert.assertEquals(freqs[w], wt.getCorpusFrequency());
			}
			ExportSettings s = new ExportSettings(null);
			s.outputDir = "./temp";
			CorpusExporter exp = new FeatureFrequencyCorpusExporter(s);
			exp.doExport(c);
			
		} catch (IOException e) {
			e.printStackTrace();
//		} catch (URISyntaxException e) {
//			e.printStackTrace();
		}
		
	}
	
	
	private Corpus readCorpus(String corpusFilename, String vocabFilename, boolean deduplicate) {
		Corpus c = new Corpus();
		URL u = this.getClass().getResource(vocabFilename);
		URL corpusUrl = this.getClass().getResource(corpusFilename);
		try {
			File f = new File(u.toURI());
			File corpusFile = new File(corpusUrl.toURI());
			ImportSettings settings = new ImportSettings(null);
			settings.dataFile = corpusFile.getCanonicalPath();
			settings.vocabFile = f.getCanonicalPath();
			settings.deduplicate = deduplicate;
			CorpusImporter.importFeatureFrequencyCorpus(c, settings);
			
			
			String[] words = FileUtils.readLinesFromFile(f.getCanonicalPath());
			
			for(int w=0;w<c.getDictionary().getNumberOfWordTypes();++w)
				Assert.assertEquals(words[w], c.getDictionary().getWordTypeById(w).getValue());
			
		} catch (IOException e) {
			e.printStackTrace();
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
		return c;
	}
}
