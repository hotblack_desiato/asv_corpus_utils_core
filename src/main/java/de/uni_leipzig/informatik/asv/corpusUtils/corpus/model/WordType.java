/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.uni_leipzig.informatik.asv.corpusUtils.corpus.model;

import java.io.Serializable;
import java.util.HashMap;

/**
 * This class comprises information on WordTypes.
 * A WordType can be seen as a class. Instances of it will be {@link WordToken}s.
 * @author Patrick Jaehnichen <jaehnichen@informatik.uni-leipzig.de>
 *
 */
public class WordType implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3342449012270133902L;
	private int id;
	private int corpusFrequency = 0;
	private HashMap<Integer, Integer> documentFrequency = new HashMap<>();
	private HashMap<String, Object> metaData;
	private String value;
	
	/**
	 * Constructs a WordType with given id and term.
	 * @param id - the id of the WordType
	 * @param term - the term which this WordType symbolizes
	 */
	public WordType(int id, String term) {
		this.id = id;
		this.value = term;
	}
	
	/**
	 * Increases the document count for this WordType.
	 * @param d - the document of which the count should be increased
	 * @param amount - amount of increase
	 */
	public void increaseDocumentFrequency(int docId, int amount) {
		Integer freq = documentFrequency.get(docId);
		if(freq == null) freq = 0;
		documentFrequency.put(docId, freq + amount);
	}

	/**
	 * Does the same as {@link increaseDocumentFrequency} but only by an amount of 1.
	 * @param d - the document of which the count is increased
	 */
	public void increaseDocumentFrequency(int docId) {
		increaseDocumentFrequency(docId, 1);
	}
	
	/**
	 * Decreases the document count for this WordType.
	 * @param d - the document of which the count will be decreased.
	 */
	public void decreaseDocumentFrequency(int docId) {
		Integer freq = documentFrequency.get(docId);
		if(freq == null) return;
		documentFrequency.put(docId, freq - 1);
	}
	
	/**
	 * Decreases the document frequency of this {@link WordType} by amount. 
	 * @param docId - the document
	 * @param amount - amount of decrease
	 */
	public void decreaseDocumentFrequency(int docId, int amount) {
		Integer freq = documentFrequency.get(docId);
		if(freq == null) return;
		if(freq - amount == 0) documentFrequency.remove(docId);
		else documentFrequency.put(docId, freq - amount);
	}

	/**
	 * Getter for the document frequency.
	 * @param docId - The document for which the frequency is to be returned
	 * @return the frequency of this WordType in the given document.
	 */
	public int getDocumentFrequency(int docId) {
		Integer freq = documentFrequency.get(docId);
		if(freq == null)
			return 0;
		return freq;
	}
	
	/**
	 * Getter for the number of documents this type appears in.
	 * @return the number of documents this type appears in
	 */
	public int getNumberOfDocumentOccurrences() {
		return documentFrequency.size();
	}
	
	/**
	 * Getter for the document ids of the documents this type appears in.
	 * @return an array of document ids
	 */
	public Integer[] getDocumentOccurrences() {
		return documentFrequency.keySet().toArray(new Integer[]{});
	}
	
	
	/**
	 * Increases the corpus frequency of this WordType.
	 * @param amount - amount to be increased
	 */
	public void increaseCorpusFrequency(int amount) {
		corpusFrequency += amount;
	}
	
	/**
	 * Increases the corpus frequency of this WordType by 1.
	 */
	public void increaseCorpusFrequency() {
		corpusFrequency++;
	}
	
	/**
	 * Decreases the corpus frequency of this WordType by 1.
	 */
	public void decreaseCorpusFrequency() {
		corpusFrequency--;
	}

	/**
	 * Decreases the corpus frequency of this WordType by amount.
	 * @param amount
	 */
	public void decreaseCorpusFrequency(int amount) {
		corpusFrequency -= amount;
	}
	
	/**
	 * Resets all frequency counts and lists to zero.
	 */
	public void resetFrequencies() {
		this.corpusFrequency = 0;
		this.documentFrequency.clear();
	}
	
	/**
	 * Getter for the frequency of this WordType in the corpus.
	 * @return this WordType's frequency in the corpus
	 */
	public int getCorpusFrequency() {
		return corpusFrequency;
	}

	/**
	 * Getter for this WordType's id
	 * @return the id of this WordType
	 */
	public int getId() {
		return id;
	}

	/**
	 * Getter for this WordType's string value.
	 * @return the term this WordType represents
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Adds some meta data object for this type.
	 * @param key - a key for the meta data  object
	 * @param val - the meta data  object
	 */
	public void addMetaData(String key, Object val) {
		if(this.metaData == null)
			this.metaData = new HashMap<>();
		this.metaData.put(key, val);
	}
	
	/**
	 * Retrieves the meta data of the given key. Automatically casts it to the expected type.
	 * @param key - the key of the meta data object
	 * @return the meta data object
	 */
	public <C> C getMetaData(String key) {
		if(this.metaData == null)
			return null;
		return (C)this.metaData.get(key);
	}
	
	/**
	 * Sets the id. This should only be called by pruning methods of Corpus
	 * object, hence it is package private.
	 * @param id - the new ID
	 */
	void setId(int id) {
		this.id = id;
	}
	
	/**
	 * Provide a string representation of this WordType, this is its string value.
	 */
	public String toString() {
		return value;
	}
	
}
