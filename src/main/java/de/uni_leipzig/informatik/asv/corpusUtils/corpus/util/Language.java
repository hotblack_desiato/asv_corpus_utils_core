/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.uni_leipzig.informatik.asv.corpusUtils.corpus.util;

import java.lang.reflect.Field;
import java.util.HashMap;

/**
 * Convenience class to keep track of language codes used by the {@link Stopwords} class.
 * @author Patrick Jaehnichen <jaehnichen@informatik.uni-leipzig.de>
 *
 */
public class Language {
	
	public static final String ARABIC = "ar";
	public static final String BULGARIAN = "bg";
	public static final String CZECH = "cs";
	public static final String DANISH = "da";
	public static final String GERMAN = "de";
	public static final String GREEK = "el"; 
	public static final String ENGLISH = "en";
	public static final String SPANISH = "es";
	public static final String ESTONIAN = "et";
	public static final String PERSIAN = "fa";
	public static final String FINNISH = "fi";
	public static final String FRENCH = "fr";
	public static final String HEBREW = "he";
	public static final String HINDI = "hi";
	public static final String CROATIAN = "hr";
	public static final String HUNGARIAN = "hu";
	public static final String ARMENIAN = "hy";
	public static final String IDO = "io";
	public static final String ICELANDIC = "is";
	public static final String ITALIAN = "it";
	public static final String JAPANESE = "ja";
	public static final String GEORGIAN = "ka";
	public static final String KHMER = "km";
	public static final String KOREAN = "ko";
	public static final String DUTCH = "nl";
	public static final String NORWEGIAN = "no";
	public static final String POLISH = "pl";
	public static final String PORTUGUESE = "pt";
	public static final String ROMANIAN = "ro";
	public static final String RUSSIAN = "ru";
	public static final String SLOVENIAN = "sl";
	public static final String SWEDISH = "sv";
	public static final String TAMIL = "ta";
	public static final String TELUGU = "te";
	public static final String THAI = "th";
	public static final String UKRAINIAN = "uk";
	public static final String VIETNAMESE = "vi";
	public static final String CHINESE = "zh";
	public static final String UNKNOWN = "u";
	
	private static HashMap<String, String> languageNames = new HashMap<String, String>();
	
	static {
		Field[] fields = Language.class.getFields();
		for(Field cur : fields) {
			if(cur.getName().equals("languageNames"))
				continue;
			try {
				String languageName = cur.getName().toLowerCase();
				String languageCode = (String) cur.get(Language.class);
				languageNames.put(languageCode, languageName);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * Getter for language name.
	 * @param languageCode - the language code
	 * @return the name of the language
	 */
	public static String getLanguageName(String languageCode) {
		return languageNames.get(languageCode);
	}
	
	/**
	 * Getter for a list of all available language codes.
	 * @return a string array containing all available language codes
	 */
	public static String[] getLanguageCodeList() {
		return languageNames.keySet().toArray(new String[]{});
	}
}
