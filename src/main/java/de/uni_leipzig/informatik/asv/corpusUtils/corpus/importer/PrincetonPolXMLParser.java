/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.uni_leipzig.informatik.asv.corpusUtils.corpus.importer;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.Corpus;

public class PrincetonPolXMLParser extends DefaultHandler {
	
	private PrincetonPolXMLImporter imp;
	private Corpus c;
	private File data;
	private StringBuilder curContent = new StringBuilder();
	private String text;
	private String source;
	private Date pubDate;
	private boolean isContent = false;
	private boolean isSource = false;
	private boolean isDate;
	
	public PrincetonPolXMLParser(PrincetonPolXMLImporter imp, Corpus c) {
		data = new File(imp.props.dataFile);
		this.imp = imp;
		this.c = c;
	}
	
	void parseDocument() throws IOException {
		SAXParserFactory spf = SAXParserFactory.newInstance();
		try {
			SAXParser p = spf.newSAXParser();
			p.parse(data, this);
		} catch (ParserConfigurationException | SAXException e1) {
			e1.printStackTrace();
		}

	}

	@Override
	public void startElement(String uri, String localName, String qName,
			Attributes attributes) throws SAXException {
		curContent.setLength(0);
		if(qName.equalsIgnoreCase("field")){
			String fieldName = attributes.getValue("n"); 
			if(fieldName.equalsIgnoreCase("body"))
				isContent = true;
			if(fieldName.equalsIgnoreCase("source"))
				isSource = true;
			if(fieldName.equalsIgnoreCase("date"))
				isDate = true;
		}

	}

	@Override
	public void endElement(String uri, String localName, String qName)
			throws SAXException {
		if(qName.equalsIgnoreCase("field")) {
			if(isContent) {
				isContent = false;
				text = curContent.toString();
			}
			if(isSource) {
				isSource = false;
				source = curContent.toString();
			}
			if(isDate) {
				isDate = false;
				//parse date string to a date object
				SimpleDateFormat sdf = new SimpleDateFormat("MMM dd, yyyy, E", Locale.ENGLISH);
				try {
					pubDate = sdf.parse(curContent.toString());
				} catch (ParseException e) {
//					System.out.println("tried to parse date: '" + curContent.toString() + "'");
//					e.printStackTrace();
//					System.exit(0);
				}
			}
		}
		if(qName.equalsIgnoreCase("article")) {
			try {
				imp.createDocumentFromStringAndAddToCorpus(c, text, source, true, pubDate);
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void characters(char[] ch, int start, int length)
			throws SAXException {
		if(isContent||isDate||isSource)
			curContent.append(ch, start, length);
	}

}
