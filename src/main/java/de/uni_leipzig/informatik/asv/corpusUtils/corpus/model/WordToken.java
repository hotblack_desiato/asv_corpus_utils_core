/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.uni_leipzig.informatik.asv.corpusUtils.corpus.model;

import java.io.Serializable;

import de.uni_leipzig.informatik.asv.corpusUtils.corpus.state.WordTokenState;

/**
 * Object holding information on a word token. 
 * Every single word in every single document is a token.
 * @author Patrick Jaehnichen <jaehnichen@informatik.uni-leipzig.de>
 *
 */
public class WordToken implements Serializable{
	private static final long serialVersionUID = 5430741946277880376L;
	private WordType type;
	private Object state;
	private int indexInDocument = 0;
	
	/**
	 * Constructs a WordToken-object given the {@link WordType} and the index in the document.
	 * {@link WordType}s can be seen as classes, whereas WordTokens are instances thereof.
	 * @param type - the id of the {@link WordType}
	 * @param index - the index in the document
	 */
	public WordToken(WordType type, int index) {
		indexInDocument = index;
		this.type = type;
	}

	/**
	 * The state of this token. Only used in processing of topic models.
	 * @return the current state
	 */
	public <T> T getState() {
		return (T) state;
	}
	
	/**
	 * Sets the current state of this token.
	 * @param state - the new state
	 */
	public void setState(Object state) {
		this.state = state;
	}
	
	public WordType getType() {
		return type;
	}

	/**
	 * Getter for the {@link WordType}-ID
	 * @return the id of the {@link WordType} this token is an instance of
	 */
	public int getTypeId() {
		return type.getId();
	}
	
	/**
	 * Getter for the index in the document.
	 * @return the index of this token in the document
	 */
	public int getIndexInDocument() {
		return indexInDocument;
	}
	
	/**
	 * Creates a copy of this WordToken object.
	 */
	@Override
	public WordToken clone() {
		WordToken wt = new WordToken(type, indexInDocument);
		wt.state = state;
		return wt;
	}
}
