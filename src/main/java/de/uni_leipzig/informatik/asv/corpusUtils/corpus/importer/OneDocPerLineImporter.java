/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
/**
 * 
 */
package de.uni_leipzig.informatik.asv.corpusUtils.corpus.importer;

import java.io.IOException;
import java.util.Date;

import de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.Corpus;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.util.ImportSettings;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.utils.FileUtils;

/**
 * @author <a href="mailto:jaehnichen@informatik.uni-leipzig.de">Patrick Jaehnichen</a>
 *
 */
public class OneDocPerLineImporter extends TextImporter {

	/**
	 * @param settings
	 */
	public OneDocPerLineImporter(ImportSettings settings) {
		super(settings);
	}

	/* (non-Javadoc)
	 * @see de.uni_leipzig.informatik.asv.corpusUtils.corpus.importer.CorpusImporter#doImport(de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.Corpus)
	 */
	@Override
	public void doImport(Corpus c) throws IOException {
		String[] docs = FileUtils.readLinesFromFile(this.props.dataFile);
		for(int docCount = 0;docCount < docs.length;docCount++)
			createDocumentFromStringAndAddToCorpus(c, docs[docCount], "doc " + docCount, true, (Date[]) null);
	}

}
