/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.uni_leipzig.informatik.asv.corpusUtils.corpus.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

/**
 * Settings class used for corpus imports. The following setting are mandatory:
 * <ul>
 * <li>SOURCE_TYPE - type of source, can be any of "atmt", "elastic", "features", "nyt",
 * "wortschatz", "email", "euproject", "princeton_pol_xml" or docIdDateText</li>
 * <li>IS_DIACHRONIC - is the source a diachronic corpus</li>
 * </ul>
 * When using "docIdDateText", provide the following:
 * <ul>
 * <li>INPUT_DATE_FORMAT - the date format the input has (e.g. LCM, Twitter, Facebook, Wortschatz or the String of the date format like "yyyy-MM-dd"</li>
 * </ul>
 * When using "elastic", provide the following:
 * <ul>
 * <li>SERVER - the elastic search server</li>
 * <li>PORT - the port on which the elasticsearch server listens</li>
 * <li>INDEX - the index to search</li>
 * <li>TYPE - the type to query for</li>
 * <li>QUERY - a json query to elasticsearch</li>
 * </ul>
 * When using "features", provide the following:
 * <ul>
 * <li>DATA_FILE - the corpus file</li>
 * <li>VOCAB_FILE - the vocabulary file</li>
 * </ul>
 * When using "nyt", provide the following:
 * <ul>
 * <li>DATA_DIR - the directory with New York Times XML format files (either in
 * XML or tgz form)</li>
 * </ul>
 * When using "wortschatz", provide the following:
 * <ul>
 * <li>SERVER - the database server</li>
 * <li>PORT - the database server port</li>
 * <li>USER - the database user</li>
 * <li>PASSWORD - the user's password</li>
 * <li>DB_NAME - the name of the database</li>
 * </ul>
 * When using "atmt" or "princeton_pol_xml", provide the following:
 * <ul>
 * <li>DATA_FILE - the file containing the data</li>
 * </ul>
 * Additional settings are:
 * <ul>
 * <li>VOCAB_FILE - when using a predefined vocabulary for anything other than
 * features</li>
 * <li>PREPROCESS - overall switch to turn preprocessing on or off</li>
 * <li>SPLIT_AT_PARAGRAPHS - splits long documents at paragraphs when true</li>
 * <li>NROMALIZE_CHARACTERS - normalize special chars such as the long s in
 * firſt to s</li>
 * <li>NORMALIZE_CURRENCY - replace all $ and € symbols followed by a number
 * with "CURRENCY_REFERENCE"</li>
 * <li>REMOVE_CURRENCY - replace all $ and € symbols followed by a number with
 * "CURRENCY_REFERENCE"</li>
 * <li>NORMALIZE_NUMBERS - replace all numbers with "NUMBER_OCCURRENCE"</li>
 * <li>REMOVE_NUMBERS - replace all numbers with "NUMBER_OCCURRENCE"</li>
 * <li>REMOVE_PUNCTUATION - replace all punctuation with white-spaces</li>
 * <li>REMOVE_STOPWORDS - as the name suggests</li>
 * <li>REMOVE_DASHES - as the name suggests</li>
 * <li>USE_STEMMER - uses one of the snowball stemmers if true
 * <li>MIN_DOC_PERCENT - minimum percentage of documents in which a type may
 * occur</li>
 * <li>MAX_DOC_PERCENT - maximum percentage of documents in which a type may
 * occur</li>
 * <li>MIN_FREQ - minimum corpus frequency for a word, used for pruning</li>
 * <li>MAX_FREQ - maximum corpus frequency for a word, used for pruning</li>
 * <li>LANGUAGE - the language of the source file, if set, speeds up stop word
 * removal</li>
 * </ul>
 * 
 * @author Patrick Jaehnichen
 * 
 */
public class ImportSettings {

	public boolean preprocess,deduplicate, normalizeCharacters, normalizeNumbers,
			removeNumbers, normalizeCurrency, removeCurrency,
			removePunctuation, removeStopwords, removeDashes, useStemmer,
			splitDocsAtParagraph;
	public String server, port, user, password, dbName, nodeName, index, type,
			dataDir, dataFile, vocabFile, query, contentTagName;
	public String[] languages;
	public int minFreq, maxFreq, stages, buckets;

	private Properties props = new Properties();
	public String sourceType;
	public Boolean isDiachronic;
	public int minLength;
	public double minDocPercent;
	public double maxDocPercent;
	public double jaccardThreshold;
	public String inputDateFormat;

	/**
	 * Available settings.
	 * 
	 * @author Patrick Jaehnichen
	 * 
	 */
	public enum Settings {
		SOURCE_TYPE, IS_DIACHRONIC, USE_VOCABULARY, DEDUPLICATE, JACCARD_THRESHOLD, STAGES, BUCKETS,
		// db and elastic
		SERVER, PORT, USER, PASSWORD, DB_NAME,
		// preprocessing
		PREPROCESS, NORMALIZE_CHARACTERS, NORMALIZE_NUMBERS, REMOVE_NUMBERS, NORMALIZE_CURRENCY, REMOVE_CURRENCY, REMOVE_PUNCTUATION, REMOVE_STOPWORDS, REMOVE_DASHES, USE_STEMMER, MIN_DOC_PERCENT, MAX_DOC_PERCENT, MIN_FREQ, MAX_FREQ, MIN_LENGTH, LANGUAGES, SPLIT_DOCS_AT_PARAGRAPH,
		// elastic
		NODE_NAME, INDEX, TYPE, QUERY,
		// file based
		DATA_DIR, DATA_FILE, VOCAB_FILE,
		// html input
		CONTENT_TAG_NAME,
		// input date format
		INPUT_DATE_FORMAT;
		;
	}

	public ImportSettings(String settingsFile) throws FileNotFoundException,
			IOException {
		if (settingsFile != null) {
			FileReader fr = new FileReader(settingsFile);
			this.props.load(fr);
			fr.close();
		}
		this.server = this.props.getProperty(Settings.SERVER.toString(), "localhost");
		this.port = this.props.getProperty(Settings.PORT.toString(), "3306");
		this.user = this.props.getProperty(Settings.USER.toString(), "root");
		this.password = this.props.getProperty(Settings.PASSWORD.toString(), "root");
		this.dbName = this.props.getProperty(Settings.DB_NAME.toString(),
				"eng_news_2010_100K");
		this.preprocess = Boolean.parseBoolean(this.props.getProperty(
				Settings.PREPROCESS.toString(), "true"));
		this.deduplicate = Boolean.parseBoolean(this.props.getProperty(
				Settings.DEDUPLICATE.toString(), "false"));
		this.normalizeCharacters = Boolean.parseBoolean(this.props.getProperty(
				Settings.NORMALIZE_CHARACTERS.toString(), "false"));
		this.normalizeNumbers = Boolean.parseBoolean(this.props.getProperty(
				Settings.NORMALIZE_NUMBERS.toString(), "false"));
		this.removeNumbers = Boolean.parseBoolean(this.props.getProperty(
				Settings.REMOVE_NUMBERS.toString(), "false"));
		this.normalizeCurrency = Boolean.parseBoolean(this.props.getProperty(
				Settings.NORMALIZE_CURRENCY.toString(), "false"));
		this.removeCurrency = Boolean.parseBoolean(this.props.getProperty(
				Settings.REMOVE_CURRENCY.toString(), "false"));
		this.removePunctuation = Boolean.parseBoolean(this.props.getProperty(
				Settings.REMOVE_PUNCTUATION.toString(), "false"));
		this.removeStopwords = Boolean.parseBoolean(this.props.getProperty(
				Settings.REMOVE_STOPWORDS.toString(), "true"));
		this.removeDashes = Boolean.parseBoolean(this.props.getProperty(
				Settings.REMOVE_DASHES.toString(), "false"));
		this.splitDocsAtParagraph = Boolean.parseBoolean(this.props.getProperty(
				Settings.SPLIT_DOCS_AT_PARAGRAPH.toString(), "false"));
		this.useStemmer = Boolean.parseBoolean(this.props.getProperty(
				Settings.USE_STEMMER.toString(), "false"));
		this.minFreq = Integer.parseInt(this.props.getProperty(
				Settings.MIN_FREQ.toString(), "-1"));
		this.maxFreq = Integer.parseInt(this.props.getProperty(
				Settings.MAX_FREQ.toString(), "-1"));
		this.minLength = Integer.parseInt(this.props.getProperty(
				Settings.MIN_LENGTH.toString(), "-1"));
		this.minDocPercent = Double.parseDouble(this.props.getProperty(
				Settings.MIN_DOC_PERCENT.toString(), "-1"));
		this.maxDocPercent = Double.parseDouble(this.props.getProperty(
				Settings.MAX_DOC_PERCENT.toString(), "-1"));
		this.jaccardThreshold = Double.parseDouble(this.props.getProperty(
				Settings.JACCARD_THRESHOLD.toString(), ".95"));
		this.stages = Integer.parseInt(this.props.getProperty(
				Settings.STAGES.toString(), "5"));
		this.buckets = Integer.parseInt(this.props.getProperty(
				Settings.BUCKETS.toString(), "1000"));
		this.languages = this.props.getProperty(Settings.LANGUAGES.toString(), "en")
				.split(",");
		this.nodeName = this.props.getProperty(Settings.NODE_NAME.toString(), "atlantis");
		this.index = this.props.getProperty(Settings.INDEX.toString(), "nyt");
		this.type = this.props.getProperty(Settings.TYPE.toString(), "document");
		this.query = this.props
				.getProperty(
						Settings.QUERY.toString(),
						"{\"query\":{\"bool\":{\"must\":[{\"match_all\":{}}],\"must_not\":[],\"should\":[]}},\"from\":0,\"size\":10,\"sort\":[],\"facets\":{}}");
		this.dataDir = this.props.getProperty(Settings.DATA_DIR.toString(), "./data");
		this.dataFile = this.props.getProperty(Settings.DATA_FILE.toString(),
				"./data/corpus.dat");
		this.vocabFile = this.props.getProperty(Settings.VOCAB_FILE.toString(),
				"./data/vocabulary.dat");
		this.contentTagName = this.props.getProperty(Settings.CONTENT_TAG_NAME.toString(), "body");

		// set vocab file to null if it does not exist
		File tempVocab = new File(this.vocabFile);
		if (!tempVocab.exists())
			this.vocabFile = null;

		this.sourceType = this.props.getProperty(Settings.SOURCE_TYPE.toString(), "file");
		this.isDiachronic = Boolean.valueOf(this.props.getProperty(
				Settings.IS_DIACHRONIC.toString(), "false"));
		
		this.inputDateFormat = this.props.getProperty(Settings.INPUT_DATE_FORMAT.toString(), DateFormatUtil.DATE_FORMAT_LCM.getName());
		
		System.out.println("************** import settings summary *****************");
		System.out.println("preprocessing: " + this.preprocess);
		System.out.println("splitting documents at paragraphs: " + this.splitDocsAtParagraph);
		System.out.println("normalizing numbers: " + this.normalizeNumbers);
		System.out.println("removing numbers: " + this.removeNumbers);
		System.out.println("normalizing currency: " + this.normalizeCurrency);
		System.out.println("removing currency: " + this.removeCurrency);
		System.out.println("removing punctutation: " + this.removePunctuation);
		System.out.println("removing stopwords: " + this.removeStopwords);
		System.err.println("retaining dashes: " + !this.removeDashes);
		System.out.println("using minimum word frequency: " + this.minFreq);
		System.out.println("using maximum word frequency: " + this.maxFreq);
		System.out.println("using minimum word length: " + this.minLength);
		System.out.println("using predefined vocab file: " + (this.vocabFile != null));
		System.out.println("input date format: " + (this.inputDateFormat));
		System.out.println("************** import settings summary *****************");

	}
}
