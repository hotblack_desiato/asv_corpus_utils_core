/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.uni_leipzig.informatik.asv.corpusUtils.corpus.importer;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

import de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.Corpus;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.Document;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.WordType;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.util.ImportSettings;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.utils.FileUtils;
/**
 * Importer class for clda type stored corpora.
 * The format is:
 * numberOfFeatures feature_1:freq_1 feature_2:freq_2 ... feature_n:freq_n
 * for each document in the corpus.
 * @author Patrick Jaehnichen
 *
 */
public class FeatureFrequencyImporter extends CorpusImporter {
	
	public FeatureFrequencyImporter(ImportSettings settings) {
		super(settings);
	}
	
	/**
	 * Imports corpus data from a single file containing the data in compact feature-frequency notation (clda style)
	 * @param c the corpus object in which to import the data
	 * @throws IOException if an exception occurred in reading the file
	 */
	public void doImport(Corpus c) throws IOException {
		//read the dictionary
		c.getDictionary().readDictionaryFile(this.props.vocabFile);
		//read the document descriptions
		String[] descriptions = null;
		//TODO: descriptions and externalID files still hardcodede, need to change this
		try {
			descriptions = FileUtils.readLinesFromFile(this.props.dataDir + File.separator + "docDescriptions.txt"); 
		} catch(IOException e) {
			//probably the file does not exist, skip
		}
		//read the external IDs
		String[] externalIds = null;
		try {
			externalIds = FileUtils.readLinesFromFile(this.props.dataDir + File.separator + "externalIds.txt"); 
		} catch(IOException e) {
			//probably the file does not exist, skip
		}
		//read secondary IDs
		String[] sIds = null;
		try {
			sIds = FileUtils.readLinesFromFile(this.props.dataDir + File.separator + "secondaryIds.txt"); 
		} catch(IOException e) {
			//probably the file does not exist, skip
		}
		//read the corpus
		HashMap<WordType, Integer> docWords = new HashMap<>();
		String filename = this.props.dataFile;
		int counter = 0;
		for(String line : FileUtils.readLinesFromFile(filename)) {
			parseDocument(c, docWords, line);
			String desc = filename;
			if(descriptions != null)
				desc = descriptions[counter];
			Document d = c.createDocumentAndAdd(docWords, desc);
			if(externalIds != null)
				d.setExternalId(externalIds[counter]);
			if(sIds != null)
				d.setSecondaryId(Integer.valueOf(sIds[counter]).intValue());
			docWords.clear();
			counter++;
		}
	}

	protected void parseDocument(Corpus c, HashMap<WordType, Integer> docWords, String line) {
		String[] parts = line.split("\\s");
		for(String word : parts) {
			if(word.indexOf(":") == -1) continue;
			String[] wordParts = word.split(":");
			int typeId = Integer.parseInt(wordParts[0]);
			WordType wt = c.getWordType(typeId);
			if(wt == null) {
				System.err.println("word type not found, id: " + typeId);
				continue;
			}
			docWords.put(wt, Integer.valueOf(wordParts[1]));
		}
	}
	
}
