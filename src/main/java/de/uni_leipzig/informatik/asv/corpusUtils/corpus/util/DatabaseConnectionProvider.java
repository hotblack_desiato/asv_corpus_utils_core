/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.uni_leipzig.informatik.asv.corpusUtils.corpus.util;

import java.net.ConnectException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Properties;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.transport.client.PreBuiltTransportClient;

import com.mongodb.DB;
import com.mongodb.MongoClient;

/**
 * Provides connections to various data sources.
 * @author patrick
 *
 */
public class DatabaseConnectionProvider {

	private static HashMap<ImportSettings, Connection> mysqlConnectionPool = new HashMap<ImportSettings, Connection>();
	
	private static TransportClient elasticsearchClient;
	
	
	private static Logger logger = LogManager.getLogger();
	
	/**
	 * Gets the current mysql connection or creates a new one.
	 * @param settings - the import settings
	 * @return the mysql connection
	 * @throws ConnectException
	 */
	public static Connection getMysqlConnection(ImportSettings settings) throws ConnectException {
		if(mysqlConnectionPool.get(settings) == null) {
			Connection c = loadDatabaseConnection(settings);
			if(c == null)
				throw new ConnectException("Could not connect to database...");
			mysqlConnectionPool.put(settings, c);
		}
		return mysqlConnectionPool.get(settings);
	}
	
	private static Connection loadDatabaseConnection(ImportSettings props) {
		//load the mysql driver
		try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			String connectionUrl = "jdbc:mysql://" + props.server + "/" + props.dbName;
			Properties conProps = new Properties();
			conProps.setProperty("user", props.user);
			conProps.setProperty("password", props.password);
			conProps.setProperty("useUnicode", "true");
			return DriverManager.getConnection(connectionUrl, conProps);
		} catch (ClassNotFoundException e) {
			logger.fatal("error loading mysql driver", e);
		} catch (InstantiationException e) {
			logger.fatal("error loading mysql driver", e);
		} catch (IllegalAccessException e) {
			logger.fatal("error loading mysql driver", e);
		} catch (SQLException e) {
			logger.fatal("error connecting to server", e);
		}
		return null;
	}

	/**
	 * Returns a {@link DB} object according to the import settings.
	 * @param settings - the import settings
	 * @return the MongoDB {@link DB} object
	 */
	public static DB getMongoDBConnection(ImportSettings settings) {
		try {
			MongoClient client = new MongoClient(settings.server, Integer.parseInt(settings.port));
			return client.getDB(settings.dbName);
		} catch (NumberFormatException | UnknownHostException e) {
			logger.fatal("error connecting to mongo db", e);
		}
		return null;
	}
	
	/**
	 * Returns the current Elasticsearch {@link Client} or creates a new one.
	 * @param settings - the import settings
	 * @return the Elasticsearch {@link Client}
	 */
	public static Client getElasticsearchClient(ImportSettings settings) {
		if(elasticsearchClient == null) {
			logger.info("no connection to elasticsearch present, connecting...");
			try {
				elasticsearchClient = new PreBuiltTransportClient(Settings.EMPTY).addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName(settings.server), Integer.valueOf(settings.port)));
			} catch (NumberFormatException | UnknownHostException e) {
				System.out.println("host unknown");
				e.printStackTrace();
			}
		}
		return elasticsearchClient;
	}
	
	/**
	 * Closes the Elasticsearch {@link Client}.
	 */
	public static void closeElasticsearchClient() {
		elasticsearchClient.close();
	}
}
