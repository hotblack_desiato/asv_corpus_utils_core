/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.uni_leipzig.informatik.asv.corpusUtils.corpus.importer;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;

import de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.Corpus;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.WordType;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.util.ImportSettings;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.utils.FileUtils;

/**
 * Importer class for timestamped clda type stored corpora
 * The format is:
 * number of timestamps
 * timestamp_1
 * number of documents in timestamp_1
 * numberOfFeatures feature_1:freq_1 feature_2:freq_2 ... feature_n:freq_n
 * for each document in timestamp_1
 * timestamp_2
 * number of documents in timestamp_2
 * ...
 * timestamp_T
 * number of documents in timestamp_T
 * numberOfFeatures feature_1:freq_1 feature_2:freq_2 ... feature_n:freq_n
 * 
 * @author Patrick Jaehnichen
 *
 */
public class DiachronicFeatureFrequencyImporter extends
		FeatureFrequencyImporter {

	public DiachronicFeatureFrequencyImporter(ImportSettings settings) {
		super(settings);
	}
	
	@Override
	/**
	 * Do the import.
	 */
	public void doImport(Corpus c) throws IOException {
		String filename = this.props.dataFile;
		String[] lines = FileUtils.readLinesFromFile(filename);
		int counter = 0;
		HashMap<WordType, Integer> docWords = new HashMap<>();
		
		int numDates = Integer.parseInt(lines[counter++]);
		for(int t=0;t<numDates;t++) {
			long timestamp = Double.valueOf(lines[counter++]).longValue() * 1000 + 60*60*2*1000; //add two hours to be sure to cross midnight (some timestamps are 23:59:30 if they mean the next day)
			Date d = new Date(timestamp);
			this.logger.info("reading date " + d.toString());
			int numDocs = Integer.parseInt(lines[counter++]);
			for(int doc=0;doc<numDocs;doc++) {
				String data = lines[counter++];
				parseDocument(c, docWords, data);
				if(docWords.size() == 0) {
					System.out.println("warning: document empty: " + data);
					continue;
				}
				c.createDocumentAndAdd(docWords, filename, d);
				docWords.clear();
			}
		}
		
	}
}
