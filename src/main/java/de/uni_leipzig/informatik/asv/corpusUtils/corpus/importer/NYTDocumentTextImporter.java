/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.uni_leipzig.informatik.asv.corpusUtils.corpus.importer;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;

import org.apache.commons.compress.archivers.tar.TarArchiveEntry;
import org.apache.commons.compress.archivers.tar.TarArchiveInputStream;
import org.apache.commons.compress.compressors.gzip.GzipCompressorInputStream;

import com.nytlabs.corpus.NYTCorpusDocument;
import com.nytlabs.corpus.NYTCorpusDocumentParser;

import de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.Corpus;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.state.DocumentState;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.state.WordTokenState;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.util.ImportSettings;

public class NYTDocumentTextImporter extends TextImporter {

	public NYTDocumentTextImporter(ImportSettings settings) {
		super(settings);
	}

	private NYTCorpusDocumentParser parser = new NYTCorpusDocumentParser();
	

	
	/**
	 * Imports a directory containing files from the New York Times Annotated corpus.
	 * Files are expected to follow the defined form.
	 * 
	 * @param c the corpus object in which to import the data
	 * @throws IOException if reading one of the files fails
	 */
	@Override
	public void doImport(Corpus c) throws IOException {
		String filename = props.dataDir;
		File file = new File(filename);
		if(!file.isDirectory())
			throw new IllegalArgumentException("Please provide a directory, " + filename + " is not a directory.");
		
		File[] files = collectFiles(filename);
		byte[] content;
		for(File f : files) {
			//these are normal files
			if(!f.getName().endsWith(".tgz")) {
				processDocument(f, c);
				System.out.println("done with file '" + f.getName() + "'");
				continue;
			}
			//otherwise these are tared and gzipped files
			TarArchiveInputStream tarIn = new TarArchiveInputStream(new GzipCompressorInputStream(new FileInputStream(f)));
			TarArchiveEntry tarEntry = tarIn.getNextTarEntry();
			while(tarEntry != null) {
				if(tarEntry.getSize() != 0 && tarEntry.isFile()) {
					//build content array
					content = new byte[(int)tarEntry.getSize()];
					//read into content array
					tarIn.read(content);
					//construct input stream
					InputStream is = new ByteArrayInputStream(content);

					processDocument(is, c, tarEntry.getName());
				}
				tarEntry = tarIn.getNextTarEntry();
			}
			tarIn.close();
			System.out.println("done with file '" + f.getAbsolutePath() + "'");
		}
	}

	protected void processDocument(InputStream is, Corpus c, String source) throws UnsupportedEncodingException {
		NYTCorpusDocument doc = null;
		try {
			doc = parser.parseNYTCorpusDocumentFromInputStream(is, source);
		} catch (Exception e) {
			System.out.println("some general exception occurred: " + e.toString());
		}
		if(doc != null) {
			processContent(c, source, doc);
		}
	}
	
	protected void processDocument(File f, Corpus c) throws IOException {
		NYTCorpusDocument doc = null;
		try {
			doc = parser.parseNYTCorpusDocumentFromFile(f, false);
		} catch (Exception e) {
			System.out.println("some general exception occurred: " + e.toString());
		} 
		if(doc != null) {
			processContent(c, f.getName(), doc);
		}
	}

	protected void processContent(Corpus c, String source, NYTCorpusDocument doc) throws UnsupportedEncodingException {
		String body = doc.getBody();
		//ignore empty bodies
		if(body == null || body.trim().equals(""))
			return;
		if(!NYTDocumentTextImporter.validUTF8(body.getBytes()))
			System.out.println("body contains malformed strings\n" + body);
		createDocumentFromStringAndAddToCorpus(c, body, doc.getHeadline(), true, doc.getPublicationDate());
	}
	
	/**
	 * Check for valid UTF-8 char sequence
	 * @param input - char sequence to test as byte array
	 * @return true if valid UTF-8, false otherwise
	 */
	public static boolean validUTF8(byte[] input) {
		  int i = 0;
		  // Check for BOM
		  if (input.length >= 3 && (input[0] & 0xFF) == 0xEF
		    && (input[1] & 0xFF) == 0xBB & (input[2] & 0xFF) == 0xBF) {
		   i = 3;
		  }

		  int end;
		  for (int j = input.length; i < j; ++i) {
		   int octet = input[i];
		   if ((octet & 0x80) == 0) {
		    continue; // ASCII
		   }

		   // Check for UTF-8 leading byte
		   if ((octet & 0xE0) == 0xC0) {
		    end = i + 1;
		   } else if ((octet & 0xF0) == 0xE0) {
		    end = i + 2;
		   } else if ((octet & 0xF8) == 0xF0) {
		    end = i + 3;
		   } else {
		    // Java only supports BMP so 3 is max
		    return false;
		   }

		   while (i < end) {
		    i++;
		    octet = input[i];
		    if ((octet & 0xC0) != 0x80) {
		     // Not a valid trailing byte
		     return false;
		    }
		   }
		  }
		  return true;
		 }
	
}
