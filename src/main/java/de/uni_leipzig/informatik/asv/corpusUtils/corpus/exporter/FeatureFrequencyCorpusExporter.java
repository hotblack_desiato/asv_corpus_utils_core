/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.uni_leipzig.informatik.asv.corpusUtils.corpus.exporter;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.TreeMap;

import cern.colt.map.OpenIntIntHashMap;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.Corpus;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.Document;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.util.ExportSettings;

/**
 * Exporter class to store the corpus a feature frequency format.
 * This is also sometimes known as SVMLight or c-lda format and looks like this:
 * 
 * number_of_types type_1:freq_1 type_2:freq_2 ... type_n:freq_n
 * 
 * One line represents one document, hence the file will be {@link Corpus.getNumberOfDocuments} lines long.
 * The types are represented as a list where the position in the list corresponds to the id of the type.
 * 
 * @author Patrick Jaehnichen <jaehnichen@informatik.uni-leipzig.de>
 *
 */
public class FeatureFrequencyCorpusExporter extends CorpusExporter {
	
	public FeatureFrequencyCorpusExporter(ExportSettings settings) {
		super(settings);
	}

	/**
	 *  Exports a corpus to feature frequency format.
	 *  @param c - the corpus to export
	 */
	@Override
	public void doExport(Corpus c) throws IOException {
		makeDirs();
		writeVocabulary(c);
		writeCorpus(c);
		writeSecondaryDocumentIds(c);
		writeDocumentDescriptions(c);
		writeExternalDocumentIds(c);
	}
	
	private void writeExternalDocumentIds(Corpus c) throws IOException {
		StringBuilder sb = new StringBuilder();
		int eIdCount = 0;
		for(Document d : c.getDocuments()) {
			if(d.getExternalId() != null)
				eIdCount++;
			sb.append(d.getExternalId());
			sb.append("\n");
		}
		//only write this file if there were external IDs
		if(eIdCount > 0) {
			BufferedWriter bw = new BufferedWriter(new FileWriter(this.props.outputDir + File.separator + "externalIDs.txt"));
			bw.write(sb.toString());
			bw.close();
		}
	}

	private void writeSecondaryDocumentIds(Corpus c) throws IOException {
		StringBuilder sb = new StringBuilder();
		int sIdCount = 0;
		for(Document d : c.getDocuments()) {
			if(d.getSecondaryId() != -1)
				sIdCount++;
			sb.append(d.getSecondaryId());
			sb.append("\n");
		}
		//only write this file if there were external IDs
		if(sIdCount > 0) {
			BufferedWriter bw = new BufferedWriter(new FileWriter(this.props.outputDir + File.separator + "secondaryIDs.txt"));
			bw.write(sb.toString());
			bw.close();
		}
	}
	
	private void writeDocumentDescriptions(Corpus c) throws IOException {
		StringBuilder sb = new StringBuilder();
		for(Document d : c.getDocuments()) {
			sb.append(d.getDescription());
			sb.append("\n");
		}
		BufferedWriter bw = new BufferedWriter(new FileWriter(this.props.outputDir + File.separator + "docDescriptions.txt"));
		bw.write(sb.toString());
		bw.close();
	}

	protected void writeCorpus(Corpus c) throws IOException {
		//write corpus file
		BufferedWriter bw = new BufferedWriter(new FileWriter(this.props.outputDir + File.separator + this.props.corpusFilename));
		TreeMap<Integer, String> docMap = new TreeMap<>();
		StringBuilder sb = new StringBuilder();
		for(Document d : c.getDocuments()) {
			sb.append(d.getNumberOfWordTypes());
			OpenIntIntHashMap wordMap = d.getWordFrequencies();
			for(int key : wordMap.keys().elements()) {
				sb.append(" ").append(key).append(":").append(wordMap.get(key));
			}
			sb.append("\n");
			docMap.put(new Integer(d.getId()), sb.toString());
			sb.setLength(0);
		}
		for(String s : docMap.values())
			bw.write(s);
		bw.close();
	}

	protected void writeVocabulary(Corpus c) throws IOException {
		//write vocab file
		PrintWriter bw = new PrintWriter(this.props.outputDir + File.separator + this.props.vocabFilename, "UTF-8");
		PrintWriter bwFreq = new PrintWriter(this.props.outputDir + File.separator + this.props.vocabFilename + ".freqs");
		
		for(int wt = 0;wt < c.getNumberOfWordTypes();wt++) {
			if(c.getWordType(wt) != null) {
				bw.write(c.getWordType(wt).getValue() + "\n");
				bwFreq.write(c.getWordType(wt).getCorpusFrequency() + "\n");
			} else {
				System.out.println("Warning: hit null word type");
			}
		}
		
		bw.close();
		bwFreq.close();
	}

}
