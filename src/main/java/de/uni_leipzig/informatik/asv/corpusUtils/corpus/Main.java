/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.uni_leipzig.informatik.asv.corpusUtils.corpus;

import java.io.File;
import java.io.IOException;

import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import de.uni_leipzig.informatik.asv.corpusUtils.corpus.exporter.CorpusExporter;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.exporter.FeatureFrequencyCorpusExporter;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.exporter.VocabularyExporter;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.exporter.diachronic.DiachronicFeatureFrequencyCorpusExporter;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.importer.AIFCSVImporter;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.importer.CSVDocIdDateTextImporter;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.importer.CoabCSVImporter;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.importer.CorpusImporter;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.importer.ECCOTEITextImporter;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.importer.EUProjectDescriptionImporter;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.importer.EmailTranscriptImporter;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.importer.FeatureFrequencyImporter;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.importer.HTMLFileImporter;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.importer.NYTDocumentTextImporter;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.importer.OneDocPerLineImporter;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.importer.PlainTextImporter;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.importer.PrincetonPolXMLImporter;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.importer.database.ElasticsearchCorpusImporter;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.importer.database.WortschatzDatabaseCorpusImporter;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.Corpus;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.DiachronicCorpus;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.util.CorpusSplitter;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.util.ExportSettings;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.util.ImportSettings;

public class Main {
	
	private static final String EXPORT_OPTION = "exportsettings";
	private static final String IMPORT_OPTION = "importsettings";
	
	private static final Logger logger = LogManager.getLogger();
	

	public static void main(String[] args) throws IOException, ParseException {
		
		//options are: input type, input settings
		Options o = new Options();
		o.addOption(IMPORT_OPTION, true, 
				"location of the import settings file, mandatory settings are:\n"
				+ "\tSOURCE_TYPE - one of atmt, coab, file, one_file, html, elastic, nyt, wortschatz, princeton_pol_xml, ecco_tei, docIdDateText\n"
				+ "\tIS_DIACHRONIC - if the source is a diachronic corpus\n"				
				+ "for source type file, html, ecco_tei, nyt and docIdDateText provide the following:\n"
				+ "\tDATA_DIR - the directory containing plain text/html, ecco xml files or nyt source files (either xml or tgz files)\n"
				+ "for source type docIdText provide the following:\n"
				+ "\tINPUT_DATE_FORMAT - the date format the input has (e.g. LCM, Twitter, Facebook, Wortschatz or the String of the date format like 'yyyy-MM-dd'\n"
				+ "for source type elastic, provide the following:\n"
				+ "\tSERVER - the IP adress of the elasticsearch server\n"
				+ "\tPORT - the port on which the server listens\n"
				+ "\tINDEX - the index to query\n"
				+ "\tTYPE - the type to query for\n"
				+ "\tQUERY - a comma separated list of search terms"
				+ "for source type wortschatz, provide the following:\n"
				+ "\tSERVER - the database server\n"
				+ "\tPORT - the port on which the server listens\n"
				+ "\tUSER - the database user\n"
				+ "\tPASSWORD - the user's password\n"
				+ "for source type atmt, coab, one_file, princeton_pol_xml and docIdDateText provide the following:\n"
				+ "\tDATA_FILE - the source file\n\n"
				+ "optional settings include:\n"
				+ "\tPREPROCESS - overall switch to turn preprocessing on(true) or off(false)\n"
				+ "\tNORMALIZE_CURRENCY - all $ and € signs followed by numbers are normalized to \"CURRENCY_REFERENCE\"\n"
				+ "\tNORMALIZE_NUMBERS - all numbers are normalized to \"NUMBER_OCCURRENCE\"\n"
				+ "\tREMOVE_PUNCTUATION - as the name suggests\n"
				+ "\tREMOVE_STOPWORDS - dito\n"
				+ "\tLANGUAGE - language of the source\n"
				+ "\tMIN_DOC_PERCENT - minimum percentage of documents in which a type must occur\n"
				+ "\tMAX_DOC_PERCENT - maximum percentage of documents in which a type may occur\n"
				+ "\tMIN_FREQ - minimum frequency of a type\n"
				+ "\tMAX_FREQ - maximum frequence of a type");
		o.addOption(EXPORT_OPTION, true, 
				"location of the export settings file, mandatory settings are:\n"
				+ "\tTARGET_TYPE - the type of the target export(currently only \"features\" and \"vocabulary\" is supported)\n"
				+ "\tOUTPUT_DIR - output directory\n"
				+ "if a diachronic corpus should be generated, use the following settings:\n"
				+ "\tCREATE_DIACHRONIC - if true, also creates a diachronic corpus (and also imports it first)\n"
				+ "if a training and test corpus should be generated, use the following settings:\n"
				+ "\tCREATE_VALIDATION - if true, creates a validation and training set from the given corpus, must be set to true if CREATE_TEST is true\n"
				+ "\tCREATE_TEST - if true, creates a test, validation and training set from the given corpus\n"
				+ "\tTRAINING_RATIO - float in ]0,1[, determines how much of the individual documents is used for training and testing\n"
				+ "optional features settings include:"
				+ "\tCORPUS_FILENAME - the name of the corpus file\n"
				+ "\tVOCABULARY_FILENAME - the name of the vocabulary file\n"
				+ "\tDIACHRONIC_CORPUS_FILENAME - the name of the diachronic corpus file\n"
				+ "\tGRANULARITY - granularity of the corpus, allowed values are DAILY, MONTHLY and YEARLY\n"
				+ "optional vocabulary settings include:"
				+ "\tMIN_FREQ - minimum frequency of a type\n"
				+ "\tMIN_LENGTH - minimum length of a type\n"
				+ "\tMAX_FREQ - maximum frequence of a type");
				
		CommandLineParser clp = new BasicParser();
		CommandLine cl = clp.parse(o, args);

		if(!cl.hasOption(IMPORT_OPTION) || !cl.hasOption(EXPORT_OPTION)) {
			showUsage(o);
			return;
		}
 		String importSettingsFile = cl.getOptionValue(IMPORT_OPTION);
		ImportSettings s = new ImportSettings(importSettingsFile);

		String exportSettingsFile = cl.getOptionValue(EXPORT_OPTION);
		ExportSettings e = new ExportSettings(exportSettingsFile);
		
 		boolean useDiachronic = s.isDiachronic.booleanValue();
 		
 		boolean deduplicate = s.deduplicate;
 		
 		
 		
 		Corpus c;
		if(useDiachronic)
			c = new DiachronicCorpus();
		else
			c = new Corpus();
	
		doImport(o, s, c);
		
		//??????Befor or after stopword removal etc.
		//deduplicate with minHashFunction
		
		if(deduplicate)
		{
			c.deduplicateCorpus(s.stages, s.buckets, s.jaccardThreshold);
		}
		
		
		
		doExport(o, e, c);
		
	}

	private static void doExport(Options o, ExportSettings e, Corpus c) throws IOException {
		CorpusExporter expSimple;
		CorpusExporter expDiachronic;
		switch(e.targetType) {
		case "features":
			if(e.createDiachronic)
				expDiachronic = new DiachronicFeatureFrequencyCorpusExporter(e);
			else expDiachronic = null;
			expSimple = new FeatureFrequencyCorpusExporter(e);
			break;
		case "sample":
			logger.info("collecting a sample of " + e.numSamples + " sample from the corpus");
			Corpus newCorpus = CorpusSplitter.collectRandomSample(c, e.numSamples);
			c = null;
			c = newCorpus;
			logger.info("corpus size is now " + c.getNumberOfDocuments());
			expSimple = new FeatureFrequencyCorpusExporter(e);
			expDiachronic = null;
			break;
		case "vocabulary":
			e.createDiachronic = false;
			expSimple = new VocabularyExporter(e);
			expDiachronic = null;
			break;
		default:
			expSimple = null;
			expDiachronic = null;
			break;
		}
		
		if(expSimple == null || (e.createDiachronic && expDiachronic==null)) {
			showUsage(o);
			throw new IllegalArgumentException();
		}
		if(e.createValidationSet) {
			String dir = e.outputDir;
			Corpus trainingSetSimple = new Corpus();
			Corpus validationSimple = new Corpus();

			if(e.createTestSet) {
				Corpus testSetSimple = new Corpus();
				CorpusSplitter.splitCorpus(c, trainingSetSimple, testSetSimple, validationSimple, e.trainingSetRatio, e.validationSetRatio);
				e.outputDir = dir + File.separator + "test";
				expSimple.doExport(testSetSimple);
			} else {
				CorpusSplitter.splitCorpus(c, trainingSetSimple, validationSimple, e.validationSetRatio);
			}
			e.outputDir = dir + File.separator + "train";
			expSimple.doExport(trainingSetSimple);
			e.outputDir = dir + File.separator + "validation";
			expSimple.doExport(validationSimple);
			if(e.createDiachronic && expDiachronic != null) {
				DiachronicCorpus trainingSetDiachronic = new DiachronicCorpus();
				DiachronicCorpus validationSetDiachronic = new DiachronicCorpus();
				if(e.createTestSet) {
					DiachronicCorpus testSetDiachronic = new DiachronicCorpus();
					CorpusSplitter.splitCorpus(c, trainingSetDiachronic, testSetDiachronic, validationSetDiachronic, e.trainingSetRatio, e.validationSetRatio);
					e.outputDir = dir + File.separator + "test";
					expDiachronic.doExport(testSetDiachronic);
				} else {
					CorpusSplitter.splitCorpus(c, trainingSetDiachronic, validationSetDiachronic, e.validationSetRatio);
				}
				e.outputDir = dir + File.separator + "train";
				expDiachronic.doExport(trainingSetDiachronic);
				e.outputDir = dir + File.separator + "validation";
				expDiachronic.doExport(validationSetDiachronic);
			}
		} else {
			e.outputDir += File.separator + "train";
			expSimple.doExport(c);
			if (e.createDiachronic && expDiachronic != null) {
				expDiachronic.doExport(c);
			}
		}		
	}

	private static void doImport(Options o, ImportSettings s, Corpus c)
			throws IOException {
		CorpusImporter imp;
		switch(s.sourceType) {
		case "file" :
			imp = new PlainTextImporter(s);
			break;
		case "one_file":
			imp = new OneDocPerLineImporter(s);
			break;
		case "coab" :
			imp = new CoabCSVImporter(s);
			break;
		case "html" :
			imp = new HTMLFileImporter(s);
			break;
		case "elastic": 
			imp = new ElasticsearchCorpusImporter(s);
			break;
		case "nyt":
			imp = new NYTDocumentTextImporter(s);
			break;
		case "wortschatz":
			imp = new WortschatzDatabaseCorpusImporter(s);
			break;
		case "email":
			imp = new EmailTranscriptImporter(s);
			break;
		case "euproject":
			imp = new EUProjectDescriptionImporter(s);
			break;
		case "princeton_pol_xml":
			imp = new PrincetonPolXMLImporter(s);
			break;
		case "ecco_tei":
			imp = new ECCOTEITextImporter(s);
			break;
		case "atmt":
			imp = new AIFCSVImporter(s);
			break;
		case "docIdDateText":
			imp = new CSVDocIdDateTextImporter(s);
			break;
		default:
			imp = new FeatureFrequencyImporter(s);
			logger.warn("assuming feature frequency input...");
			showUsage(o);
			break;
		}
		logger.info("importing...");
		imp.doImport(c);
		logger.info("pruning (if applicable)...");
		if(s.minFreq != -1)
			c.pruneDictionaryMinFreq(s.minFreq);
		if(s.maxFreq != -1)
			c.pruneDictionaryMaxFreq(s.maxFreq);
		if(s.minLength != -1)
			c.pruneDictionaryMinLength(s.minLength);
		if(s.minDocPercent != -1 && s.maxDocPercent != -1) {
			c.pruneDictionaryMinMaxPercent(s.minDocPercent, s.maxDocPercent);
		} else if(s.minDocPercent != -1) {
			c.pruneDictionaryMinPercent(s.minDocPercent);
		} else if(s.maxDocPercent != -1) {
			c.pruneDictionaryMaxPercent(s.maxDocPercent);
		}
		// final pruning to remove empty docs
		logger.info("\t" + c.getDocuments().size()+ " docs left");
		logger.info("perform final pruning");
		c.pruneDictionary();
		logger.info("done pruning." );
		logger.info("\t" + c.getDocuments().size()+ " docs left");
		logger.info("\t" + c.getDocumentIds().size()+ " doc IDs left");
		logger.info("\t" + c.getNumberOfWordTypes() + " types left");

		
		logger.info("done with import.");
	}
	
	private static void showUsage(Options o) {
		HelpFormatter f = new HelpFormatter();
		f.printHelp("java -jar asv-corpus-utils-{version}-jar-with-dependencies.jar", o);
		
	}
}
