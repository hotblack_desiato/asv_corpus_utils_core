/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.uni_leipzig.informatik.asv.corpusUtils.corpus.model;

import info.debatty.java.lsh.LSHMinHash;
import info.debatty.java.lsh.MinHash;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeSet;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cern.colt.map.OpenIntIntHashMap;

/**
 * The Corpus class holds information about a whole corpus. Via this class, the
 * dictionary and document list may be accessed. It also provides getter methods
 * for corpus statistics such as number of word tokens, number of word types,
 * number of document etc.
 * 
 * @author jaehnichen
 * 
 */
public class Corpus implements Serializable {
	/**
	 * serial version uid
	 */
	private static final long serialVersionUID = -8608275879939368592L;
	
	private static final Logger logger = LogManager.getLogger();

	/**
	 * the dictionary
	 */
	private Dictionary dic;

	/**
	 * the set of documents
	 */
	private Map<Integer, Document> docs;

	private int corpusSize;

	public Corpus() {
		this.dic = new Dictionary();
		this.docs = Collections
				.synchronizedMap(new LinkedHashMap<Integer, Document>());
	}

	/**
	 * Get the document set. Since the backing data structure is a hash set, the
	 * returned order of documents is not guaranteed to stay stable over time.
	 * To deliberately shuffle the document set, use
	 * {@link getShuffledDocumentSet}, to get an iterator on a shuffled list use
	 * {@link getShuffledDocumentIterator}.
	 * 
	 * @see HashSet
	 * @return the set of documents
	 */
	public Set<Document> getDocuments() {
		return new LinkedHashSet<Document>(this.docs.values());
	}

	public Set<Integer> getDocumentIds() {
		return this.docs.keySet();
	}

	/**
	 * Shuffles the documents and returns them as a Set object.
	 * 
	 * @return the set of shuffled documents
	 */
	public Set<Document> getShuffledDocumentSet() {
		return new HashSet<Document>(getShuffledDocumentList());
	}

	/**
	 * Compiles the word frequencies for a subset of documents.
	 * 
	 * @param subset
	 *            - a {@link Set} of {@link Document} objects
	 * @return the word frequencies as {@link OpenIntIntHashMap}, word ids are
	 *         keys, their frequencies are values
	 */
	public OpenIntIntHashMap getDocumentSubsetWordFrequencies(
			Set<Document> subset) {
		OpenIntIntHashMap ret = new OpenIntIntHashMap();
		for (Document d : subset) {
			OpenIntIntHashMap freqs = d.getWordFrequencies();
			for (int wt : freqs.keys().elements()) {
				ret.put(wt, ret.get(wt) + freqs.get(wt));
			}
		}
		return ret;
	}

	/**
	 * Compiles the word frequencies for a subset of documents.
	 * 
	 * @param subset
	 *            - a {@link Set} of {@link Document} IDs
	 * @return the word frequencies as {@link OpenIntIntHashMap}, word ids are
	 *         keys, their frequencies are values
	 */
	public OpenIntIntHashMap getDocumentIdSubsetWordFrequencies(
			Set<Integer> subset) {
		Set<Document> docSubSet = getDocumentsByIds(subset);
		return getDocumentSubsetWordFrequencies(docSubSet);
	}

	/**
	 * Gets the document with the specified id.
	 * 
	 * @param id
	 * @return the document with this id
	 */
	public Document getDocumentById(int id) {
		return this.docs.get(id);
	}

	/**
	 * Gets a set of documents for a set of ids.
	 * 
	 * @param ids
	 *            a set of integer ids
	 * @return the documents
	 */
	public Set<Document> getDocumentsByIds(Set<Integer> ids) {
		Set<Document> ret = new HashSet<>();
		for (Integer docId : ids)
			ret.add(getDocumentById(docId));
		return ret;
	}

	/**
	 * Getter for the dictionary of the corpus.
	 * 
	 * @return the dictionary used
	 */
	public Dictionary getDictionary() {
		return this.dic;
	}

	/**
	 * Setter for the dictionary of the corpus.
	 * 
	 * @param the
	 *            new dictionary
	 */
	public void setDictionary(Dictionary d) {
		this.dic = d;
	}

	/**
	 * This method prunes the dictionary, in case the loaded dictionary
	 * comprises a lot more terms than actually appear in the corpus. All
	 * {@link WordTypes} with corpus frequency 0 will be removed.
	 */
	public HashSet<String> pruneDictionary() {

		HashSet<Integer> typesToDelete = new HashSet<>();
		HashSet<String> deletedStrings = new HashSet<>();
		for (WordType wt : getAllWordTypes()) {
			if (wt.getCorpusFrequency() == 0) {
				typesToDelete.add(wt.getId());
				deletedStrings.add(wt.getValue());
			}
		}

		for (Document doc : getDocuments()) {
			doc.pruneTypes(typesToDelete);
		}

		for (Integer wt : typesToDelete) {
			this.dic.removeWordType(wt);
		}
		System.out.println("pruned " + typesToDelete.size() + " unused words");
		postPrune();
		return deletedStrings;
	}

	public void pruneDictionary(HashSet<String> typesToDelete) {
		HashSet<Integer> toDelete = new HashSet<>();
		for (String type : typesToDelete) {
			WordType testType = null;
			try {
				testType = getWordType(type, false);
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
			if (testType != null)
				toDelete.add(testType.getId());
		}

		for (Document doc : getDocuments()) {
			doc.pruneTypes(toDelete);
		}

		for (Integer wt : toDelete) {
			this.dic.removeWordType(wt);
		}
		postPrune();
	}

	/**
	 * Prunes the dictionary according to a minimum length of types. Can be
	 * useful to get rid of terms dr, mr etc. but use with care.
	 * 
	 * @param minimum
	 *            length of types
	 */
	public void pruneDictionaryMinLength(int minLength) {

		HashSet<Integer> typesToDelete = new HashSet<Integer>();

		for (WordType wt : getAllWordTypes()) {
			if (wt.getValue().length() < minLength)
				typesToDelete.add(wt.getId());
		}

		for (Integer wt : typesToDelete) {
			WordType type = getWordType(wt);
			this.corpusSize -= type.getCorpusFrequency();
			this.dic.removeWordType(wt);
		}

		for (Document doc : getDocuments())
			doc.pruneTypes(typesToDelete);

		System.out.println("pruned " + typesToDelete.size()
				+ " types shorter than " + minLength + " characters");

		postPrune();
	}

	/**
	 * This method prunes the dictionary. All {@link WordType}s with corpus
	 * frequency lower minFreq will be removed.
	 * 
	 * @param minFreq
	 *            - the minimal frequency of a type
	 */
	public void pruneDictionaryMinFreq(int minFreq) {
		pruneDictionaryMinMax(minFreq, true);
		postPrune();
	}

	/**
	 * This method prunes the dictionary. All {@link WordType}s with corpus
	 * frequency higher maxFreq will be removed.
	 * 
	 * @param minFreq
	 *            - the minimal frequency of a type
	 */
	public void pruneDictionaryMaxFreq(int maxFreq) {
		pruneDictionaryMinMax(maxFreq, false);
		postPrune();
	}

	/**
	 * Prunes the dictionary. All {@link WordType}s that occur in less than
	 * minDocs and in more than maxDocs will be removed.
	 * 
	 * @param minDocs
	 *            - minimum number of document in which a type may occur
	 * @param maxDocs
	 *            - maximum number of document in which a type may occur
	 */
	public void pruneDictionaryMinMaxDocs(int minDocs, int maxDocs) {
		pruneDictionaryMinMaxDocOcc(minDocs, true);
		pruneDictionaryMinMaxDocOcc(maxDocs, false);
		postPrune();
	}

	/**
	 * This method prunes the dictionary. All {@link WordType}s with document
	 * occurrence frequency lower minDocs will be removed.
	 * 
	 * @param minFreq
	 *            - the minimal frequency of a type
	 */
	public void pruneDictionaryMinDocs(int minDocs) {
		pruneDictionaryMinMaxDocOcc(minDocs, true);
		postPrune();
	}

	/**
	 * This methods prunes the dictionary. All {@link WordType}s with document
	 * occurrence frequency larger than maxDocs will be removed.
	 * 
	 * @param maxDocs
	 *            - the maximal document frequency of a type
	 */
	public void pruneDictionaryMaxDocs(int maxDocs) {
		pruneDictionaryMinMaxDocOcc(maxDocs, false);
		postPrune();
	}

	/**
	 * pruning according to document occurrences
	 * 
	 * @param freq
	 * @param min
	 */

	private void pruneDictionaryMinMax(int freq, boolean min) {

		HashSet<Integer> typesToDelete = new HashSet<Integer>();

		Set<Integer> docsConcerned = new HashSet<>();
		for (WordType wt : getAllWordTypes()) {
			if ((min && wt.getCorpusFrequency() < freq)
					|| (!min && wt.getCorpusFrequency() > freq)) {
				typesToDelete.add(wt.getId());
				this.corpusSize -= wt.getCorpusFrequency();
				for (Integer docId : wt.getDocumentOccurrences())
					docsConcerned.add(docId);
			}
		}

		for (Integer wt : typesToDelete)
			this.dic.removeWordType(wt);

		for (Integer doc : docsConcerned)
			getDocumentById(doc).pruneTypes(typesToDelete);

		System.out.println("pruned " + typesToDelete.size() + " types with "
				+ (min ? "less" : "more") + " than " + freq + " occurrences, "
				+ this.docs.size() + " documents left");
	}

	/**
	 * Experimental method. Prunes types in documents that have a tf-idf score
	 * less than threshold.
	 * 
	 * @param threshold
	 */
	public void pruneDictionaryTfIdf(double threshold) {
		double logAllDocs = Math.log(getNumberOfDocuments());
		double idf, tfidf;
		for (WordType wt : getAllWordTypes()) {
			Integer[] docs = wt.getDocumentOccurrences();
			idf = logAllDocs - Math.log(docs.length);
			for (int doc : docs) {
				Document d = getDocumentById(doc);
				int wordFreq = wt.getDocumentFrequency(doc);
				tfidf = wordFreq * idf;
				if (tfidf < threshold) {
					d.getWordFrequencies().put(wt.getId(), 0);
					wt.decreaseDocumentFrequency(doc, wordFreq);
					wt.decreaseCorpusFrequency(wordFreq);
				}

			}
		}
	}

	protected void postPrune() {
		rebuildTypeIds();
		this.dic.reorderMap();

		for (Document doc : getDocuments()) {
			if (doc.getLength() == 0)
				removeDocument(doc);
			doc.rebuildFreqList();
		}
	}

	private void pruneDictionaryMinMaxDocOcc(int freq, boolean min) {

		HashSet<Integer> typesToDelete = new HashSet<Integer>();

		for (WordType wt : getAllWordTypes()) {
			if ((min && wt.getNumberOfDocumentOccurrences() < freq)
					|| (!min && wt.getNumberOfDocumentOccurrences() > freq))
				typesToDelete.add(wt.getId());
		}

		for (Integer wt : typesToDelete) {
			WordType type = getWordType(wt);
			this.corpusSize -= type.getCorpusFrequency();
			this.dic.removeWordType(wt);
		}

		for (Document doc : getDocuments())
			doc.pruneTypes(typesToDelete);

		System.out.println("pruned " + typesToDelete.size() + " types with "
				+ (min ? "less" : "more") + " than " + freq
				+ " docs in which they occur");
	}

	/**
	 * Rebuilds the type ids so that they are zero based. Helps to use type ids
	 * as array indices.
	 */
	public void rebuildTypeIds() {
		int count = 0;
		for (WordType type : this.getAllWordTypes()) {
			type.setId(count);
			count++;
		}
	}

	/**
	 * Shuffles the document set and returns an iterator on the shuffled
	 * sequence of documents.
	 * 
	 * @return an iterator to the shuffled document set
	 */
	public Iterator<Document> getShuffledDocumentIterator() {
		return getShuffledDocumentList().iterator();
	}

	private ArrayList<Document> getShuffledDocumentList() {
		ArrayList<Document> docList = new ArrayList<>(
				this.docs.values());
		Collections.shuffle(docList);
		return docList;
	}

	/**
	 * Returns the {@link WordType}-object in this corpus representing the given
	 * term.
	 * 
	 * @param s
	 *            - the term
	 * @param expand
	 *            - true, if the dictionary is allowed to grow if an unknown
	 *            term is queried, false otherwise
	 * @return the {@link WordType}-object of the given term
	 * @throws UnsupportedEncodingException
	 */
	public synchronized WordType getWordType(String s, boolean expand)
			throws UnsupportedEncodingException {
		return this.dic.getWordTypeByString(s, expand);
	}

	/**
	 * Returns the {@link WordType}-object in this corpus with the given id. The
	 * id corresponds with the position in the vocabulary.
	 * 
	 * @param id
	 *            - the id of the {@link WordType}-object
	 * @return the {@link WordType}-object represented by the given id
	 */
	public WordType getWordType(int id) {
		return this.dic.getWordTypeById(id);
	}

	/**
	 * Adds a document to the corpus. To be used internally, called by {@link Document} constructor.
	 * The {@link Document} should be the prefered method to add a document to the corpus.
	 * 
	 * @param d
	 *            - the document to add
	 */
	synchronized void addDocument(Document d) {
		d.setId(getNumberOfDocuments());
		this.docs.put(d.getId(), d);
	}

	/**
	 * Removes a document from the corpus.
	 * 
	 * @param the
	 *            document
	 */
	public void removeDocument(Document d) {
		this.docs.remove(d.getId());
	}

	/**
	 * Getter for the number of documents in the corpus.
	 * 
	 * @return the number of documents in the corpus
	 */
	public int getNumberOfDocuments() {
		return this.docs.size();
	}

	/**
	 * Getter for the size of the dictionary.
	 * 
	 * @return the number of distinct {@link WordType}-objects in the dictionary
	 */
	public int getNumberOfWordTypes() {
		return this.dic.getNumberOfWordTypes();
	}

	/**
	 * Getter for the size of the corpus.
	 * 
	 * @return the number of {@link WordToken}s in the corpus
	 */
	public int getNumberOfWordTokens() {
		return this.corpusSize;
	}

	/**
	 * Getter for all {@link WordType}s
	 * 
	 * @return a collection containing all {@link WordType}s in the dictionary
	 */
	public Collection<WordType> getAllWordTypes() {
		return this.dic.getAllWordTypes();
	}

	/**
	 * Getter for maximum document length.
	 * 
	 * @return the maximal number of tokens in a document of this corpus
	 */
	public int getMaximumDocumentSize() {
		int max = 0;
		for (Document d : getDocuments())
			max = d.getNumberOfWordTypes() > max ? d.getNumberOfWordTypes()
					: max;
		return max;
	}

	void increaseCorpusSize() {
		this.corpusSize++;
	}

	void increaseCorpusSize(int amount) {
		this.corpusSize += amount;
	}

	void decreaseCorpusSize() {
		this.corpusSize--;
	}

	void decreaseCorpusSize(int amount) {
		this.corpusSize -= amount;
	}

	/**
	 * Creates a document from the given frequency map and adds it to the
	 * corpus. The frequency map should contain wordTypeIds from
	 * {@link WordType} as keys and the corresponding frequencies as values. The
	 * meta array of strings contains meta data for a document, by definition,
	 * the first element of this array contains the name of the source file.
	 * 
	 * @param words
	 *            - the frequency map
	 * @param meta
	 *            - meta data for the document
	 * @throws WordTypeNotFoundException
	 *             when one of the word types could not be found
	 */
	public Document createDocumentAndAdd(HashMap<WordType, Integer> words,
			String source, Date... d) {
		Document doc = new Document(this, source, d);
		for (Entry<WordType, Integer> entry : words.entrySet()) {
			try {
				doc.addWord(entry.getKey(), entry.getValue());
			} catch (WordTypeNotFoundException e) {
				System.err.println("could not find type with id="
						+ entry.getKey() + ", ignoring");
				continue;
			}
		}

		return doc;
	}

	/**
	 * Creates an empty document and adds it to the corpus. The meta array of
	 * strings contains meta data for a document, by definition, the first
	 * element of this array contains the name of the source file.
	 * 
	 * @param meta
	 *            - meta data for the document
	 * @throws WordTypeNotFoundException
	 *             when one of the word types could not be found
	 */
	public Document createDocumentAndAdd(String source, Date... d) {
		return new Document(this, source, d);
	}

	/**
	 * Adds a {@link Set} of {@link Document}s to the corpus.
	 * 
	 * @param docs
	 */
	public void addDocuments(Set<Document> docs) {
		for (Document d : docs) {
			for (int key : d.getWordFrequencies().keys().elements()) {
				int count = d.getWordFrequencies().get(key);
				WordType wt = getWordType(key);
				wt.increaseCorpusFrequency(count);
				wt.increaseDocumentFrequency(d.getId(), count);
				increaseCorpusSize(count);
			}
		}
	}

	/**
	 * Creates a deep copy of this corpus object.
	 */
	public Corpus clone() {
		Corpus c = new Corpus();
		c.dic = this.dic.clone();
		c.docs = Collections.synchronizedMap(new HashMap<Integer, Document>());
		for (Integer doc : this.docs.keySet())
			c.docs.put(doc, this.docs.get(doc).clone(c));
		c.corpusSize = this.corpusSize;
		return c;
	}

	/**
	 * This method prunes the dictionary. All {@link WordTypes} with corpus
	 * frequency lower prunePercentMin will be removed.
	 * 
	 * @param minPercent
	 *            - the minimal document frequency of a type in percent
	 */
	public void pruneDictionaryMinPercent(double minPercent) {
		pruneDictionaryMinMaxPercent(minPercent, true);
		postPrune();
	}

	/**
	 * This method prunes the dictionary. All {@link WordTypes} with corpus
	 * frequency higher maxFreq will be removed.
	 * 
	 * @param maxPercent
	 *            - the maximum document frequency of a type in percent
	 */
	public void pruneDictionaryMaxPercent(double maxPercent) {
		pruneDictionaryMinMaxPercent(maxPercent, false);
		postPrune();
	}

	/**
	 * This method prunes the dictionary. All {@link WordTypes} with relative
	 * document frequency of lower than minPercent and higher than maxPercent
	 * will be removed. I.e. words that appear in less than minPercent % of the
	 * documents and in more than maxPercent % of the documents are removed.
	 * 
	 * @param maxPercent
	 *            - the maximum document frequency of a type in percent
	 */
	public void pruneDictionaryMinMaxPercent(double minPercent,
			double maxPercent) {
		pruneDictionaryMinMaxPercent(minPercent, true);
		pruneDictionaryMinMaxPercent(maxPercent, false);
		postPrune();
	}

	private void pruneDictionaryMinMaxPercent(double prunePercent, boolean min) {
		int originalTypeCount = getAllWordTypes().size();
		HashSet<Integer> typesToDelete = new HashSet<>();

		for (WordType wt : getAllWordTypes()) {
			double wtPercent = (double) wt.getNumberOfDocumentOccurrences()
					/ (double) this.docs.size();
			if ((min && wtPercent < prunePercent)
					|| (!min && wtPercent > prunePercent))
				typesToDelete.add(wt.getId());
		}
		for (Integer wt : typesToDelete) {
			WordType type = getWordType(wt);
			this.corpusSize -= type.getCorpusFrequency();
			this.dic.removeWordType(wt);
		}
		System.out.println("pruned " + typesToDelete.size() + " types (of "
				+ originalTypeCount + ") with " + (min ? " less " : " more ")
				+ "then " + prunePercent + " document occurrences");

		for (Document doc : getDocuments())
			doc.pruneTypes(typesToDelete);

	}

	public void deduplicateCorpus(int stages, int buckets,
			double jaccardThreshold) {
		// TEST PURPOS
		int comparison = 0;
		int startDocs = this.getNumberOfDocuments();
		logger.info("deduplication with: " + stages + " Stages, "
				+ buckets + " Buckets, " + jaccardThreshold
				+ " Threshold for Jaccard");

		HashMap<Integer, HashSet<Integer>> candidates = new HashMap<>();
		LSHMinHash lsh = new LSHMinHash(stages, buckets,
				this.getNumberOfWordTypes());

		Set<Document> docs = this.getDocuments();
		boolean[] words;
		int documentsHashed = 0;
		for (Document d : docs) {
			words = d.getBinarySparseVector();

			int[] hash = lsh.hashSignature(words);
			d.setHashBuckets(hash);

			for (int stage = 0; stage < stages; stage++) {
				int stageHash = hash[stage];

				HashSet<Integer> oldVal = candidates.get(stageHash);
				if (oldVal == null) {
					HashSet<Integer> temp = new HashSet<Integer>();
					temp.add(d.getId());
					candidates.put(stageHash, temp);
				} else {
					oldVal.add(d.getId());
					// candidates.put(stageHash, oldVal);
				}

			}
			if(documentsHashed%100==0)
				logger.debug("deduplication, created hashes for " + documentsHashed + " documents");
			documentsHashed++;
		}
		int maxBuckets = 0;
		double avBuckets = 0;
		
		for(Integer hash : candidates.keySet()) {
			int candidateBuckets = candidates.get(hash).size();
			if(candidateBuckets > maxBuckets)
				maxBuckets = candidateBuckets;
			avBuckets += candidateBuckets;
		}
		avBuckets /= candidates.size();
			
		
		logger.info(
				"deduplication, created hashes for all documents, found "
						+ candidates.size() + " deletion candidates, max #buckets is " + maxBuckets + ", av #buckets is " + avBuckets);
		
		HashSet<Integer> DocumentsForDeletion = new HashSet<>();
		int candidatesProcessed = 0;
		// now check candidates for duplicate entries with detailed method
		for (Integer hash : candidates.keySet()) {
			// similarity based on Jaccard/Cosine
			Integer[] compareCandidates = candidates.get(hash).toArray(
					new Integer[0]);

			// Important to keep item with lowest number
			Arrays.sort(compareCandidates);

			if (compareCandidates.length < 2)
				continue;

			// walk on upper.tri of the similarity matrix
			for (int i = 0; i < compareCandidates.length - 1; i++) {
				Document d1 = this.getDocumentById(compareCandidates[i]);

				if (d1 == null)
					continue;

				TreeSet<Integer> bspv1 = d1.getSortedTypeList();

				for (int j = i + 1; j < compareCandidates.length; j++) {

					Document d2 = this.getDocumentById(compareCandidates[j]);

					if (d2 == null)
						continue;

					TreeSet<Integer> bspv2 = d2.getSortedTypeList();

					double sim = MinHash.jaccardIndex(bspv1, bspv2);

					comparison++;

					// TODO:Add csv writer for export of id pair table
					if (sim > jaccardThreshold)// Delete second candidate
												// because its got a higher id
						DocumentsForDeletion.add(d2.getId());
				}
			}
			if(candidatesProcessed%100==0)
				logger.debug("deduplication, done processing with " + candidatesProcessed + " candidates");
			candidatesProcessed++;
		}
		logger.info("deduplication, found " + DocumentsForDeletion.size() + " duplicate documents, deleting...");

		for (Integer id : DocumentsForDeletion) {
			this.removeDocument(this.getDocumentById(id));
		}

		logger.info("deduplication is done with " + comparison
				+ " Operations in minHash LSH. In " + comparison
				/ (Math.pow(startDocs, 2) / 2) * 100 + " percent of baseline. "
				+ DocumentsForDeletion.size() + " Documents of " + startDocs
				+ " where deleted.");

		// sort by date
		// keep first
	}
}
