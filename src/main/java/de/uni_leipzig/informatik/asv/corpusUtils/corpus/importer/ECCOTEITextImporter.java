/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
/**
 * 
 */
package de.uni_leipzig.informatik.asv.corpusUtils.corpus.importer;

import java.io.File;
import java.io.IOException;
import java.util.Date;

import nu.xom.Builder;
import nu.xom.Nodes;
import nu.xom.ParsingException;
import nu.xom.XPathContext;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.Corpus;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.Document;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.state.DocumentState;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.state.WordTokenState;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.util.ImportSettings;

/**
 * @author <a href="mailto:jaehnichen@informatik.uni-leipzig.de">Patrick Jaehnichen</a>
 *
 */
public class ECCOTEITextImporter extends TextImporter {
	public static final String TEI_NAMESPACE = "http://www.tei-c.org/ns/1.0";
	public static final String TEI_NAMESPACE_PREFIX = "t";
	public static final String TITLE_XPATH = "/" + TEI_NAMESPACE_PREFIX + ":TEI/" 
									+ TEI_NAMESPACE_PREFIX + ":teiHeader/" 
									+ TEI_NAMESPACE_PREFIX + ":fileDesc/"
									+ TEI_NAMESPACE_PREFIX + ":titleStmt/"
									+ TEI_NAMESPACE_PREFIX + ":title"; 
	public static final String AUTHOR_XPATH = "/" + TEI_NAMESPACE_PREFIX + ":TEI/"
												+ TEI_NAMESPACE_PREFIX + ":teiHeader/"
												+ TEI_NAMESPACE_PREFIX + ":fileDesc/"
												+ TEI_NAMESPACE_PREFIX + ":titleStmt/"
												+ TEI_NAMESPACE_PREFIX + ":author"; 
	public static final String TEXT_XPATH = "/" + TEI_NAMESPACE_PREFIX + ":TEI/" + TEI_NAMESPACE_PREFIX + ":text"; 
	
	/**
	 * @param settings
	 */
	public ECCOTEITextImporter(ImportSettings settings) {
		super(settings);
	}

	/* (non-Javadoc)
	 * @see de.uni_leipzig.informatik.asv.corpusUtils.corpus.importer.CorpusImporter#doImport(de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.Corpus)
	 */
	@Override
	public void doImport(Corpus c) throws IOException {
		File[] eccoFiles = collectFiles(this.props.dataDir);
		String content = "", title = "", author = "";
		Builder b = new Builder();
		XPathContext ctx = new XPathContext(TEI_NAMESPACE_PREFIX, TEI_NAMESPACE);
		int skipped = 0, processed = 0;
		for(File eccoFile : eccoFiles) {
			if(!eccoFile.getAbsolutePath().endsWith(".xml"))
				continue;
			processed++;
			try {
				nu.xom.Document xmlDoc = b.build(eccoFile);
				Nodes titleMatches = xmlDoc.getRootElement().query(TITLE_XPATH, ctx);
				if(titleMatches.size() == 0) {
					logger.warn("current document has not title, skipping");
					skipped++;
					continue;
				}
				title = titleMatches.get(0).getValue();
				
				Nodes authorMatches = xmlDoc.getRootElement().query(AUTHOR_XPATH, ctx);
				if(authorMatches.size() > 0)
					author = authorMatches.get(0).getValue();
				
				if(props.splitDocsAtParagraph) {
					Nodes paragraphs = xmlDoc.getRootElement().query("//" + TEI_NAMESPACE_PREFIX + ":div", ctx);
					for(int i=0;i<paragraphs.size();i++) {
						content = paragraphs.get(i).getValue();
						Document d = createDocumentFromStringAndAddToCorpus(c, content, title + " [par. " + (i+1) + "]", true, (Date[]) null);
						d.setExternalId(eccoFile.getName() + " [par. " + (i+1) + "]");
					}
				} else {
					Nodes fullText = xmlDoc.getRootElement().query(TEXT_XPATH, ctx);
					if(fullText.size() == 0) {
						logger.warn("current document has no content, skipping");
						skipped++;
						continue;
					}
					content = fullText.get(0).getValue();
					Document d = createDocumentFromStringAndAddToCorpus(c, content, title, true, (Date[]) null);
					d.setExternalId(eccoFile.getName());
				}
			} catch (ParsingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if(processed%100==0)
				logger.info("done with " + processed + " of " +  eccoFiles.length + " files");
		}
		logger.debug("skipped " + skipped + " documents of " + eccoFiles.length);

	}
	
	public static void main(String args[]) throws Exception {
		XPathContext ctx = new XPathContext(TEI_NAMESPACE_PREFIX, TEI_NAMESPACE);
		File[] testFiles = collectFiles("/Users/patrick/Data/ecco_collection/test_in");
		Builder b = new Builder();
		for(File f : testFiles) {
			nu.xom.Document doc = b.build(f);
			Nodes ps = doc.getRootElement().query("//" + TEI_NAMESPACE_PREFIX + ":div", ctx);
			System.out.println("found " + ps.size() + " p nodes in file " + f.getAbsolutePath());
		}
	}
}
