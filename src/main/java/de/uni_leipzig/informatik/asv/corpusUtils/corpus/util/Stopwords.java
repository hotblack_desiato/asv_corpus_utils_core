/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.uni_leipzig.informatik.asv.corpusUtils.corpus.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.security.CodeSource;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.Vector;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import org.apache.commons.lang3.ArrayUtils;

import com.ibm.icu.text.CharsetMatch;

import de.uni_leipzig.informatik.asv.corpusUtils.helper.utils.FileUtils;

/**
 * Utility class for maintaining stop word files.
 * @author Patrick Jaehnichen <jaehnichen@informatik.uni-leipzig.de>
 *
 */
public class Stopwords {
	
	private static HashMap<String, String[]> stopWords = new HashMap<>();
	private static Set<String> allStopWords = new HashSet<>();

	static {
		try {
			HashMap<String, String> stopwordFileContents = new HashMap<>();
			
			//IDE case
			URL stopWordDir = Stopwords.class.getResource(".");
			if(stopWordDir != null) {
				File s = new File(stopWordDir.getFile());
				File[] stopWordFiles = s.listFiles(new FilenameFilter() {
					public boolean accept(File dir, String name) {
						if(name.endsWith("class"))
							return false;
						return true;
					}
				});
				String line = "";
				StringBuffer sb = new StringBuffer();
				for(int i=0;i<stopWordFiles.length;i++) {
					CharsetMatch cm = FileUtils.getCharsetMatch(stopWordFiles[i].getAbsolutePath());
					Reader r = cm.getReader();
					if(r == null) {
						r = new FileReader(stopWordFiles[i]);
					}
					BufferedReader br = new BufferedReader(r);
					while((line = br.readLine()) != null) {
						sb.append(line).append("\n");
					}
					stopwordFileContents.put(stopWordFiles[i].getName(), sb.toString());
					sb.setLength(0);
				}
			} else {
				//compiled jar case
				CodeSource src = Stopwords.class.getProtectionDomain().getCodeSource();
				if (src != null) {
				  URL jar = src.getLocation();
				  File zFile = new File(jar.getFile());
				  ZipFile zipFile = new ZipFile(zFile);
				  Enumeration<ZipEntry> zEntries = (Enumeration<ZipEntry>) zipFile.entries();
				  while(zEntries.hasMoreElements()) {
					  ZipEntry ze = zEntries.nextElement();
					  String name = ze.getName();
					  if(name.contains("corpusUtils/corpus/util/") && !name.endsWith(".class") && !name.endsWith("/")) {
						  String lang = name.substring(name.lastIndexOf("/")+1);
						  StringBuffer fileContent = new StringBuffer();
						  
						  BufferedReader br = new BufferedReader(new InputStreamReader(zipFile.getInputStream(ze), "UTF-8"));
						  String line = "";
						  while((line = br.readLine()) != null) {
							  fileContent.append(line).append("\n");
						  }
						  br.close();
						  stopwordFileContents.put(lang, fileContent.toString());
					  }
				  }
				  zipFile.close();
				} 
				else {
				  /* Fail... */
					System.err.println("could not read stopword files");
				}			
			}
			Vector<String> currentStopWords = new Vector<>();
			for(String lang : stopwordFileContents.keySet()) {
				String language = "en";
				String charSet = "UTF-8";
				String[] lines = stopwordFileContents.get(lang).split("\n");
				for(String line : lines) {
					if(line.startsWith("#")) {
						int parenthesisPos = line.indexOf("(");
						if(parenthesisPos > 0) {
							language = line.substring(parenthesisPos+1, parenthesisPos+3);
						}
						line = line.toLowerCase();
						if(line.matches("charset:") || line.matches("encoding:")) {
							line.replaceAll("#", "");
							line.replaceAll("charset:", "");
							line.replaceAll("encoding:", "");
							line = line.trim();
							charSet = line;
						}
					}
					if(!line.equals("") && !line.startsWith("#")) {
						line = new String(line.getBytes(charSet), charSet);
						currentStopWords.add(line);
						allStopWords.add(line);
					}
				}
				stopWords.put(language, currentStopWords.toArray(new String[currentStopWords.size()]));
				currentStopWords.clear();
			}
		} catch(IOException e) {
			e.printStackTrace();
			System.err.println("could not read stopWord files");
		}
	}
	
	/**
	 * Checks in any available language if the given word is a stopword.
	 * @param word
	 * @return true if word is a stopword, false otherwise
	 */
	public static boolean isGenericStopWord(String word) {
		return allStopWords.contains(word);
	}
	
	/**
	 * Decide whether the given word is a stopword in the given language.
	 * @param language 
	 * @param word
	 * @return true if the word is a stopword, false otherwise
	 */
	public static boolean isStopWord(String language, String word) {
//		if(!isInitialized) init();
		language = language.toLowerCase();
		String[] _stopWords = stopWords.get(language);
		if(_stopWords != null) {
			return ArrayUtils.contains(_stopWords, word);
		}
		return isStopWord(word);
	}
	
	public static void loadStopwords(String language, String filename) throws UnsupportedEncodingException, IOException {
		CharsetMatch cm = FileUtils.getCharsetMatch(filename);
		BufferedReader br = new BufferedReader(cm.getReader());
		String charSet = cm.getName();
		String line = "";
		ArrayList<String> _stopWords = new ArrayList<String>();
		while((line = br.readLine()) != null) {
			if(!line.equals("") && !line.startsWith("#")) {
				line = new String(line.getBytes(charSet), charSet);
				_stopWords.add(line);
			}
		}
		stopWords.put(language, _stopWords.toArray(new String[]{}));
	}
	
	/**
	 * Works like {@link isStopWord} but scans all available languages.
	 * If it is found in any language, search is aborted and the word counts as a stopword.
	 * @param word
	 * @return true if the is a stopword in any language available, false otherwise
	 */
	private static boolean isStopWord(String word) {
		for(String[] stopWordList : stopWords.values()) {
			if(ArrayUtils.contains(stopWordList, word))
				return true;
		}
		return false;
	}
}
