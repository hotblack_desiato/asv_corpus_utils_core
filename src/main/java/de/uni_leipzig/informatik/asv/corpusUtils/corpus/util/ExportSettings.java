/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.uni_leipzig.informatik.asv.corpusUtils.corpus.util;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

import de.uni_leipzig.informatik.asv.corpusUtils.corpus.exporter.diachronic.DiachronicFeatureFrequencyCorpusExporter.Granularity;

/**
 * Settings for export.
 * Mandatory settings are:
 * <ul>
 * 	<li>TARGET_TYPE - type of export, currently, only "features", "sample" and "vocabulary" is supported</li>
 * 	<li>OUTPUT_DIR - as the name suggests</li>
 * 	<li>CREATE_DIACHRONIC - create a diachronic export as well (if possible)</li>
 * </ul>
 * If using "features" as TARGET_TYPE, provide the following:
 * <ul>
 * 	<li>CORPUS_FILENAME - the name of the exported corpus file</li>
 * 	<li>VOCABULARY_FILENAME - the name of the exported vocabulary file</li>
 * 	<li>DIACHRONIC_CORPUS_FILENAME - the name of the exported diachronic corpus (if applicable)</li>
 * 	<li>GRANULARITY - granularity of the corpus, allowed values are DAILY, MONTHLY and YEARLY</li>
 * </ul>
 * If using "sample" as TARGET_TYPE, provide the following:
 * <ul>
 * 	<li>SAMPLE_SIZE</li>
 * </ul>
 * If using "vocabulary" as TARGET_TYPE, provide the following:
 * <ul>
 * 	<li>VOCABULARY_FILENAME - the name of the exported vocabulary file</li>
 * 	<li>MIN_FREQ - minimum frequency of a type</li>
 * 	<li>MIN_LENGTH - minimum length of a type</li>
 * 	<li>MAX_FREQ - maximum frequency of a type</li>
 * @author <a href="mailto:jaehnichen@informatik.uni-leipzig.de">Patrick Jaehnichen</a>
 *
 */
public class ExportSettings {	
	/**
	 * Available settings.
	 * @author Patrick Jaehnichen
	 *
	 */
	public enum Settings {
		OUTPUT_DIR,
		CORPUS_FILENAME,
		VOCABULARY_FILENAME,
		DIACHRONIC_CORPUS_FILENAME,
		CREATE_DIACHRONIC,
		GRANULARITY,
		TARGET_TYPE,
		MIN_FREQ,
		MIN_LENGTH,
		MAX_FREQ,
		SAMPLE_SIZE,
		CREATE_TEST,
		CREATE_VALIDATION,
		TRAINING_RATIO, 
		VALIDATION_RATIO;
	}
	
	private Properties props = new Properties();
	public String outputDir;
	public String corpusFilename;
	public String vocabFilename;
	public String diachronicCorpusFilename;
	public String targetType;
	public boolean createDiachronic, createTestSet, createValidationSet;
	public Granularity granularity;
	public int maxFreq, minFreq, minLength, numSamples;
	public double trainingSetRatio;
	public double validationSetRatio;
	
	public ExportSettings(String configFile) throws FileNotFoundException, IOException {
		if(configFile != null) {
			FileReader fr = new FileReader(configFile);
			this.props.load(fr);
			fr.close();
		}
		this.outputDir = this.props.getProperty(Settings.OUTPUT_DIR.toString(), "./out");
		this.corpusFilename = this.props.getProperty(Settings.CORPUS_FILENAME.toString(), "corpus.dat");
		this.vocabFilename = this.props.getProperty(Settings.VOCABULARY_FILENAME.toString(), "vocabulary.dat");
		this.diachronicCorpusFilename = this.props.getProperty(Settings.DIACHRONIC_CORPUS_FILENAME.toString(), "corpus_diachronic.dat");
		this.targetType = this.props.getProperty(Settings.TARGET_TYPE.toString(), "features");
		this.createDiachronic = Boolean.valueOf(this.props.getProperty(Settings.CREATE_DIACHRONIC.toString(), "false")).booleanValue();
		this.granularity = Granularity.valueOf(this.props.getProperty(Settings.GRANULARITY.toString(), "DAILY"));
		this.minFreq = Integer.valueOf(this.props.getProperty(Settings.MIN_FREQ.toString(), "0")).intValue();
		this.minLength = Integer.valueOf(this.props.getProperty(Settings.MIN_LENGTH.toString(), "0")).intValue();
		this.maxFreq = Integer.valueOf(this.props.getProperty(Settings.MAX_FREQ.toString(), "-1")).intValue();
		if(this.maxFreq == -1)
			this.maxFreq = Integer.MAX_VALUE;
		this.numSamples = Integer.valueOf(this.props.getProperty(Settings.SAMPLE_SIZE.toString(), "500")).intValue();
		this.createTestSet = Boolean.valueOf(this.props.getProperty(Settings.CREATE_TEST.toString(), "false")).booleanValue();
		this.createValidationSet = Boolean.valueOf(this.props.getProperty(Settings.CREATE_VALIDATION.toString(), "false")).booleanValue();
		this.trainingSetRatio = Double.valueOf(this.props.getProperty(Settings.TRAINING_RATIO.toString(), ".7")).doubleValue();
		this.validationSetRatio = Double.valueOf(this.props.getProperty(Settings.VALIDATION_RATIO.toString(), ".2")).doubleValue();

	}
}
