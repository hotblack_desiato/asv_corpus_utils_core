/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.uni_leipzig.informatik.asv.corpusUtils.corpus.model;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.TreeSet;

public class DiachronicCorpus extends Corpus {

	/**
	 * serial version uid 
	 */
	private static final long serialVersionUID = 2502620233805283609L;
	
	private SortedMap<Date, Set<Integer>> documentDateAssociation = Collections.synchronizedSortedMap(new TreeMap<Date, Set<Integer>>());
	
	private Set<Integer> usedYears = Collections.synchronizedSet(new TreeSet<Integer>());

	private Set<Integer> usedMonths = Collections.synchronizedSet(new TreeSet<Integer>());

	private Set<Integer> usedDays = Collections.synchronizedSet(new TreeSet<Integer>());

	/**
	 * Returns all document ids of documents that have this year in their state.
	 * @param year
	 * @return a set of document ids
	 */
	public Set<Integer> getDocumentsByYear(int year) {
		return buildCollection(getDailyDocumentsByYear(year));
	}
	
	private Set<Integer> buildCollection(Set<Set<Integer>> docs) {
		Set<Integer> ret = new HashSet<Integer>();
		for(Set<Integer> s : docs)
			ret.addAll(s);
		return ret;
	}

	/**
	 * Returns all document ids of documents that have this year in their state on a daily basis.
	 * @param year
	 * @return an {@link ArrayList} of sets of document ids
	 */
	public Set<Set<Integer>> getDailyDocumentsByYear(int year) {
		Calendar c = Calendar.getInstance();
		c.clear();
		c.set(Calendar.YEAR, year);
		Date start = c.getTime();
		c.set(Calendar.YEAR, year+1);
		Date end = c.getTime();
		return getDocs(start, end);
	}
	
	/**
	 * Returns all document ids of documents that have this year and this month in their state.
	 * @param month
	 * @param year
	 * @return a set of document ids
	 */
	public Set<Integer> getDocumentsByMonthAndYear(int month, int year) {
		return buildCollection(getDailyDocumentsByMonthAndYear(month, year));
	}

	/**
	 * Returns all document ids of documents that have this year and this month in their state on a daily basis.
	 * @param month
	 * @param year
	 * @return an {@link ArrayList} of sets of document ids
	 */
	public Set<Set<Integer>> getDailyDocumentsByMonthAndYear(int month, int year) {
		Calendar c = Calendar.getInstance();
		c.clear();
		c.set(Calendar.YEAR, year);
		c.set(Calendar.MONTH, month);
		Date start = c.getTime();
		if(month == 12) {
			c.set(Calendar.YEAR, year+1);
			c.set(Calendar.MONTH, 1);
		} else {
			c.set(Calendar.MONTH, month+1);
		}
		Date end = c.getTime();
		
		return getDocs(start, end);
	}
	
	/**
	 * Returns all document ids of documents that appeared between the given dates. Start date inclusive, end date exclusive.
	 * @param start date
	 * @param end date
	 * @return a set of document ids of documents appearing within the given dates
	 */
	public Set<Integer> getDocumentsBetween(Date start, Date end) {
		return buildCollection(getDocs(start, end));
	}
	
	/**
	 * Returns all document ids of documents that have this year and this month and this day in their state.
	 * @param month
	 * @param year
	 * @param day
	 * @return a set of document ids
	 */
	public Set<Integer> getDocumentsByDayAndMonthAndYear(int day, int month, int year) {
		Calendar c = Calendar.getInstance();
		c.clear();
		c.set(year, month, day);
		return this.documentDateAssociation.get(c.getTime());
	}
	
	@Override
	synchronized void addDocument(Document d) {
		super.addDocument(d);
		if(d.getPublicationDate() == null) {
			//two options: a) try to load a Corpus into a DiachrinicCorpus object, b) missing publication date
			return;
		}
		//track used dates
		Calendar c = Calendar.getInstance();
		c.setTime(d.getPublicationDate());
		int year = c.get(Calendar.YEAR);
		int month = c.get(Calendar.MONTH);
		int day = c.get(Calendar.DAY_OF_MONTH);
		c.clear();
		c.set(year, month, day);
		this.usedDays.add(day);
		this.usedMonths.add(month);
		this.usedYears.add(year);
		
		if(this.documentDateAssociation.get(c.getTime()) == null) {
			Set<Integer> s = new HashSet<>();
			s.add(d.getId());
			this.documentDateAssociation.put(c.getTime(), s);
		} else {
			this.documentDateAssociation.get(c.getTime()).add(d.getId());
		}
	}

	@Override
	public Corpus clone() {
		DiachronicCorpus c = (DiachronicCorpus) super.clone();
		c.documentDateAssociation.putAll(this.documentDateAssociation);
		return c;
	}
	
	/**
	 * Returns a {@link Set} of Integer values representing the years used in this corpus.
	 * @return the usedYears
	 */
	public Set<Integer> getUsedYears() {
		return this.usedYears;
	}

	/**
	 * Returns a {@link Set} of Integer values representing the months used in this corpus.
	 * @return the usedMonths
	 */
	public Set<Integer> getUsedMonths() {
		return this.usedMonths;
	}

	/**
	 * Returns a {@link Set} of Integer values representing the days used in this corpus.
	 * @return the usedDays
	 */
	public Set<Integer> getUsedDays() {
		return this.usedDays;
	}

	private Set<Set<Integer>> getDocs(Date start, Date end) {
		HashSet<Set<Integer>> ret = new HashSet<>();
		for(Set<Integer> subSetAtDate : this.documentDateAssociation.subMap(start, end).values()) {
			ret.add(subSetAtDate);
		}
		return ret;
	}
	/**
	 * Get the number of different dates for which documents are present
	 * @return the number of dates
	 */
	public int getNumberOfDates() {
		return this.documentDateAssociation.size();
	}
	
	/**
	 * Get the number of documents in this corpus having a certain date
	 * @param d the date to query
	 * @return the number of documents
	 */
	public int getNumberOfDocumentsForDate(Date d) {
		return this.documentDateAssociation.get(d).size();
	}
	
	/**
	 * Gets a collection of {@link Date} objects used in this corpus.
	 * @return a collection of {@link Date}s
	 */
	public Set<Date> getUsedDates() {
		return this.documentDateAssociation.keySet();
	}
	
	/**
	 * Get the IDs of documents in this corpus having a certain date.
	 * @param d the date to query
	 * @return a {@link Set} of document IDs
	 */
	public Set<Integer> getDocumentsForDate(Date d) {
		return this.documentDateAssociation.get(d);
	}

	@Override
	public void removeDocument(Document d) {
		Iterator<Date> dateIt = this.documentDateAssociation.keySet().iterator();
		while(dateIt.hasNext()) {
			Date date = dateIt.next();
			this.documentDateAssociation.get(date).remove(d.getId());
			if(this.documentDateAssociation.get(date).size() == 0)
				dateIt.remove();
		}
		super.removeDocument(d);
	}

	@Override
	protected void postPrune() {
		super.postPrune();
		this.usedDays.clear();
		this.usedMonths.clear();
		this.usedYears.clear();
		//track used dates
		Calendar c = Calendar.getInstance();
		for(Document dd : getDocuments()) {
			if(dd.getPublicationDate() == null)
				continue;
			c.setTime(dd.getPublicationDate());
			int year = c.get(Calendar.YEAR);
			int month = c.get(Calendar.MONTH);
			int day = c.get(Calendar.DAY_OF_MONTH);
			c.clear();
			c.set(year, month, day);
			this.usedDays.add(day);
			this.usedMonths.add(month);
			this.usedYears.add(year);
		}
	}
	
	

}
