/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.uni_leipzig.informatik.asv.corpusUtils.corpus.importer;

import java.io.File;
import java.io.IOException;

import de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.Corpus;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.util.ImportSettings;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.utils.FileUtils;

/**
 * Importer class for plain text files.
 * @author Patrick Jaehnichen
 *
 */
public class PlainTextImporter extends TextImporter {
	
	public PlainTextImporter(ImportSettings settings) {
		super(settings);
	}

	/**
	 * Imports a directory of plaintext files. Each file should contain exactly one document.
	 * @param c the corpus object in which to import the data
	 * @throws IOException if reading a file fails
	 */
	@Override
	public void doImport(Corpus c) throws IOException {
		String filename = props.dataDir;
		File f = new File(filename);
		if(!f.isDirectory())
			throw new IllegalArgumentException("Please provide a directory, " + filename + " is not a directory.");
		File[] files = collectFiles(filename);
		for(File file : files) {
			String docString = FileUtils.readLineFromFile(file.getAbsolutePath());
			createDocumentFromStringAndAddToCorpus(c, docString, file.getName(), true);
		}
	}
	

}
