/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.uni_leipzig.informatik.asv.corpusUtils.corpus.model;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.ConnectException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import de.uni_leipzig.informatik.asv.corpusUtils.corpus.util.DatabaseConnectionProvider;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.util.ImportSettings;

/**
 * The dictionary class holds information on the {@link WordType} used in a corpus.
 * @author Patrick Jaehnichen <jaehnichen@informatik.uni-leipzig.de>
 *
 */
public class Dictionary implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2496521290064230302L;
	private Map<Integer, WordType> dic;
	private Map<String, Integer> dicReverse;
	
	public static final Comparator<Map.Entry<Integer, WordType>> WORD_FREQUENCY_COMPARATOR = new Comparator<Map.Entry<Integer, WordType>>() {

		public int compare(Map.Entry<Integer, WordType> obj1,
				Map.Entry<Integer, WordType> obj2) {

			int result = 0;

			Map.Entry<Integer, WordType> e1 = obj1;
			Map.Entry<Integer, WordType> e2 = obj2;// Sort based on values.

			Integer value1 = e1.getValue().getCorpusFrequency();
			Integer value2 = e2.getValue().getCorpusFrequency();

			if (value1.compareTo(value2) == 0) {

				String word1 = e1.getValue().getValue();
				String word2 = e2.getValue().getValue();

				// Sort String in an wordNumber order
				result = word1.compareTo(word2);

			} else {
				// Sort values in a descending order
				result = value2.compareTo(value1);
			}

			return result;
		}
	};
	
	/**
	 * Constructor for {@link Dictionary}
	 */
	public Dictionary() {
		this.dic = Collections.synchronizedMap(new TreeMap<Integer, WordType>());
		this.dicReverse = Collections.synchronizedMap(new TreeMap<String, Integer>());
	}
	
	/**
	 * Reorders the underlying maps. Useful when manually altering the dictionary map without
	 * changing the reverse map.
	 */
	public void reorderMap() {
		ArrayList<Entry<Integer, WordType>> oldList = new ArrayList<>(this.dic.entrySet());
		
		this.dic = Collections.synchronizedMap(new TreeMap<Integer, WordType>());
		this.dicReverse = Collections.synchronizedMap(new TreeMap<String, Integer>());
		
		for(Entry<Integer, WordType> e : oldList)
		{
			int id = e.getValue().getId();
			
			this.dic.put(id, e.getValue());
			this.dicReverse.put(e.getValue().toString(), id);	
		}
	}
	
	/**Sorts the Dictionray by frequency and returns a sorted list of word types
	 * @param asc - sets the order of the returned map
	 * @return
	 */
	public List<WordType> frequencySortedWordTypes(boolean asc)
	{
		List<Entry<Integer, WordType>> oldList = new ArrayList<>(this.dic.entrySet());
		
		Collections.sort(oldList, Dictionary.WORD_FREQUENCY_COMPARATOR);
		
		if(asc)
		{
			Collections.reverse(oldList);
		}
		
		List<WordType> result = new ArrayList<>();
		
		for(Entry<Integer, WordType> e : oldList)
		{
			result.add(e.getValue());
		}
		
		return result;
	}
	
	/**
	 * Gets a {@link WordType} object based on its {@link String} representation.
	 * If no type is found, the dictionary may be expanded depending on the expand parameter.
	 * @param s - the string representation
	 * @param expand - allow dictionary to grow
	 * @return the {@link WordType} for this string, or null if not found and not allowed to grow
	 * @throws UnsupportedEncodingException
	 */
	public synchronized WordType getWordTypeByString(String s, boolean expand) throws UnsupportedEncodingException {
		Integer id = this.dicReverse.get(s);
		if(id == null) {
			if(!expand)
				return null;
			id = this.dicReverse.size();
			this.dicReverse.put(s, id);	
		}
		WordType wt = this.dic.get(id);
		if(wt == null) {
			wt = new WordType(id, s);
			this.dic.put(id, wt);
		}
		return wt;
	}
	
	/**
	 * Gets the {@link WordType} by ID.
	 * @param id
	 * @return the {@link WordType}
	 */
	public WordType getWordTypeById(int id) {
		return this.dic.get(id);
	}
	
	/**
	 * Reads the dictionary from a text file.
	 * @param source file
	 * @throws IOException
	 */
	public int readDictionaryFile(String filename) throws IOException {
		String line = "";
		int linenumber = -1;
		BufferedReader br = new BufferedReader(new FileReader(filename));
		while((line = br.readLine()) != null) {
			linenumber++;
			if(line.isEmpty()){
				System.out.println("skipping empty line at line " + linenumber);
				continue;
			}
			this.dic.put(linenumber, new WordType(linenumber, line.trim()));
			this.dicReverse.put(line.trim(), linenumber);
		}
		br.close();
		return linenumber;
	}
	
	/**
	 * Reads the dictionary from a (MySQL) database.
	 * @param settings
	 * @throws UnsupportedEncodingException
	 */
	public void readDictionaryFromDatabase(ImportSettings settings) throws UnsupportedEncodingException {
		try {
			Connection c = DatabaseConnectionProvider.getMysqlConnection(settings);
			//select only one term expressions and lower case by default
			String sql = "select distinct lower(word) from words where word not like \"% %\"";
			//remove punctuation when specified
			if(settings.removePunctuation) 
				sql += " and word not regexp '^[[:punct:]]$'";
			//remove certain frequency ranges when specified
			if(settings.minFreq != -1)
				sql += " and freq >= " + settings.minFreq;
			if(settings.maxFreq != -1)
				sql += " and freq <= " + settings.maxFreq;
			if(settings.minLength != -1)
				sql += " and char_length(word) >= " + settings.minLength;
			System.out.print("reading dictionary...");
			PreparedStatement stmt = c.prepareStatement(sql);
			ResultSet rs = stmt.executeQuery();
			int counter = 0;
			while(rs.next()) {
				String word = rs.getString(1);
				this.dic.put(counter, new WordType(counter, word));
				this.dicReverse.put(word, counter);
				counter++;
			}
			rs.close();
			stmt.close();
			System.out.print(" done, read " + getNumberOfWordTypes() + " word types.\n");
		} catch (ConnectException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	/**
	 * Read dictionary from provided HashMap.
	 */
	public void readDictionaryFromHashMap(HashMap<Integer, WordType> source) {
		for(Entry<Integer, WordType> e : source.entrySet()) {
			this.dic.put(e.getKey(), e.getValue());
			this.dicReverse.put(e.getValue().getValue(), e.getKey());
		}
	}
	
	/**
	 * Gets the number of different {@link WordType}s in the dictionary (its size)
	 * @return the size of the dictionary
	 */
	public int getNumberOfWordTypes() {
		return this.dic.size();
	}
	
	/**
	 * Gets all {@link WordType}s in the dictionary.
	 * @return a {@link Collection} of {@link WordType}s
	 */
	public Collection<WordType> getAllWordTypes() {
		return this.dic.values();
	}
	
	/**
	 * Removes a {@link WordType} from the dictionary.
	 * @param wt
	 */
	public void removeWordType(Integer wt) {
		
		//first remove mapping
		this.dicReverse.remove(this.dic.get(wt).getValue());
		
		this.dic.remove(wt);
		
		
		
		
	}
	
	/**
	 * Generate a deep copy of this {@link Dictionary} object.
	 */
	public Dictionary clone() {
		Dictionary d = new Dictionary();
		d.dic = Collections.synchronizedMap(new HashMap<Integer, WordType>()); 
		d.dic.putAll(this.dic);
		for(Integer id : d.dic.keySet())
			d.dic.get(id).resetFrequencies();
		d.dicReverse = Collections.synchronizedMap(new HashMap<String, Integer>()); 
		d.dicReverse.putAll(this.dicReverse);
		return d;
	}

}
