/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.uni_leipzig.informatik.asv.corpusUtils.corpus.importer;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.List;

import net.htmlparser.jericho.Attribute;
import net.htmlparser.jericho.Config;
import net.htmlparser.jericho.Element;
import net.htmlparser.jericho.Segment;
import net.htmlparser.jericho.Source;
import net.htmlparser.jericho.StartTag;
import net.htmlparser.jericho.Tag;

import org.apache.commons.lang3.StringEscapeUtils;

import cern.colt.Arrays;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.Corpus;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.state.DocumentState;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.state.WordTokenState;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.util.ImportSettings;

public class HTMLFileImporter extends TextImporter {

	public HTMLFileImporter(ImportSettings settings) {
		super(settings);
	}

	@Override
	public void doImport(
			Corpus c)
			throws IOException {
		File inputDir = new File(props.dataDir);
		if(!inputDir.isDirectory())
			throw new IllegalArgumentException("Please provide a directory, " + inputDir.getAbsolutePath() + " is not a directory.");
		File[] files = collectFiles(inputDir.getAbsolutePath(), createFileTypeFileFilter(".html"));
		String[] tag_id = props.contentTagName.split("#");
		logger.info(Arrays.toString(tag_id));
		for(File file : files) {
//			logger.info("processing file " + file.getAbsolutePath());
			Source s = new Source(file);
//			s.setLogger(null);
			String title = file.getName();
			String content = "";
			List<StartTag> titleTags = s.getAllStartTags("title");
			for(Tag tTag : titleTags) {
				title += " - " + StringEscapeUtils.unescapeHtml4(tTag.getElement().getContent().toString());
			}
//			for (Tag seg : s.getAllStartTags(tag_id[0])) {
//				logger.info(seg.getElement());
//			}
			Element contentTag = s.getAllStartTags(tag_id[0]).get(0).getElement();
//			logger.info(contentTag);
			if(tag_id.length == 2) {
				contentTag = s.getElementById(tag_id[1]);
			}
//			logger.info("content: " + contentTag);
//			for(Tag tag : tags) {
			
			if(contentTag == null){
				logger.error("no content tag for file " + file.getAbsolutePath());
			} else
				content = StringEscapeUtils.unescapeHtml4(contentTag.getContent().getTextExtractor().toString());
//			}
			createDocumentFromStringAndAddToCorpus(c, content, title, true, (Date[]) null);
		}
	}

}
