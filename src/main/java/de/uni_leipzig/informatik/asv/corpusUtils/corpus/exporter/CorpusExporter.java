/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.uni_leipzig.informatik.asv.corpusUtils.corpus.exporter;

import java.io.File;
import java.io.IOException;

import de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.Corpus;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.util.ExportSettings;

/**
 * Base class for all classes providing an export of the given corpus to some format.
 * @author Patrick Jaehnichen <jaehnichen@informatik.uni-leipzig.de>
 *
 */
public abstract class CorpusExporter {

	protected ExportSettings props;

	/**
	 * @param settings
	 */
	public CorpusExporter(ExportSettings settings) {
		this.props = settings;
	}
	
	/**
	 * Exports the given Corpus to some target system. All implementing classes have access to the {@link ExportSettings} 
	 * given at instantiation and can utilize them to further specify the output system.
	 * @param c - the {@link Corpus} to export
	 * @throws IOException if export failed
	 */
	public abstract void doExport(Corpus c) throws IOException;
	
	/**
	 * Convenience method to ensure that output directory (if export is file based) really exists.
	 */
	public void makeDirs() {
		//make sure all parent directories exist
		File f = new File(this.props.outputDir);
		f.mkdirs();
	}


}
