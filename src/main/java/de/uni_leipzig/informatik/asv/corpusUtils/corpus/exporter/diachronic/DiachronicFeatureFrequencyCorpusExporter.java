/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.uni_leipzig.informatik.asv.corpusUtils.corpus.exporter.diachronic;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;
import java.util.TreeSet;

import cern.colt.map.OpenIntIntHashMap;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.exporter.FeatureFrequencyCorpusExporter;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.importer.DiachronicFeatureFrequencyImporter;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.Corpus;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.DiachronicCorpus;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.Document;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.util.ExportSettings;

/**
 * Exporter for diachronic clda format.
 * For a description of this format see {@link DiachronicFeatureFrequencyImporter}.
 * @author Patrick Jaehnichen
 *
 */
public class DiachronicFeatureFrequencyCorpusExporter extends FeatureFrequencyCorpusExporter {

	/**
	 * if true, timestamps are stored as seconds since the epoch (1.1.1970), otherwise
	 * a custom format of yyyy-MM-dd'T'HH:mm:ss is used
	 */
	public boolean useLongDate = true;
	
	private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.ENGLISH);

	/**
	 * if true, also exports the underlying vocabulary
	 */
	public boolean writeVocab = true;
	
	/**
	 * Granularity of the data, i.e. store every day/month/year as a timestamp.
	 * @author <a href="mailto:jaehnichen@informatik.uni-leipzig.de">Patrick Jaehnichen</a>
	 *
	 */
	public enum Granularity {
		DAILY, MONTHLY, YEARLY;
	}
	
	/**
	 * Constructor.
	 * @param s - the {@link ExportSettings}
	 */
	public DiachronicFeatureFrequencyCorpusExporter(ExportSettings s) {
		super(s);
	}
	
	/**
	 * Do the actual export. Writes the vocabulary (if applicable) and the corpus
	 * to the output directory as given in the {@link ExportSettings}.
	 * @param c - the corpus to export 
	 */
	@Override
	public void doExport(Corpus c) throws IOException {
		makeDirs();
		if(this.writeVocab)
			super.writeVocabulary(c);
		writeCorpus(c);
	}

	/* (non-Javadoc)
	 * @see de.uni_leipzig.informatik.asv.corpusUtils.corpus.exporter.FeatureFrequencyCorpusExporter#writeCorpus(de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.Corpus, java.lang.String)
	 */
	@Override
	protected void writeCorpus(Corpus c) throws IOException {
		DiachronicCorpus d = (DiachronicCorpus) c;
		
		Set<Date> allDates = getDatesAccordingToGranularity(d);
		
		
		
		int processedDates = 0, docsSize;
		
		StringBuilder finalStringBuilder = new StringBuilder();
		StringBuilder docStringBuilder = new StringBuilder();
		for(Date date : allDates) {
			Set<Integer> docs = getDocsAccordingToGranularity(d, date);
			if(docs.size() == 0)
				continue;
			processedDates++;
//			System.out.println("exporting date: " + date.toString());
			if(this.useLongDate) 
				finalStringBuilder.append(date.getTime()/1000f).append("\n"); //use seconds since epoch
			else
				finalStringBuilder.append(constructDateString(date)).append("\n");
			
			docsSize = 0;
			
			for(Integer docId : docs) {
				Document doc = c.getDocumentById(docId.intValue());
				if(doc == null) {
					System.out.println("warning: document with id " + docId + " does not exist");
					continue;
				}
				docsSize++;
				docStringBuilder.append(doc.getNumberOfWordTypes());
				OpenIntIntHashMap wordMap = doc.getWordFrequencies();
				for(int key : wordMap.keys().elements()) {
					docStringBuilder.append(" ").append(key).append(":").append(wordMap.get(key));
				}
				docStringBuilder.append("\n");
			}
			finalStringBuilder.append(docsSize).append("\n").append(docStringBuilder.toString());
			
			docStringBuilder.setLength(0);
		}
		
		BufferedWriter out = new BufferedWriter(new FileWriter(this.props.outputDir + File.separator + this.props.diachronicCorpusFilename));
		out.write(processedDates + "\n");
		out.write(finalStringBuilder.toString());
		out.close();
	}

	private Set<Integer> getDocsAccordingToGranularity(
			DiachronicCorpus d, Date date) {
		Set<Integer> docIds = new HashSet<>();
		Calendar c = Calendar.getInstance();
		c.clear();
		c.setTime(date);
		switch (this.props.granularity) {
		case DAILY:
			docIds = d.getDocumentsForDate(date);
			break;
		case MONTHLY:
			docIds = d.getDocumentsByMonthAndYear(c.get(Calendar.MONTH), c.get(Calendar.YEAR));
			break;
		case YEARLY:
			docIds = d.getDocumentsByYear(c.get(Calendar.YEAR));
			break;
		default:
			docIds = d.getDocumentsForDate(date);
			break;
		}
		return docIds;
	}

	private Set<Date> getDatesAccordingToGranularity(DiachronicCorpus d) {
		Set<Date> dates = new TreeSet<>();
		Set<Integer> years = d.getUsedYears();
		Calendar c = Calendar.getInstance();
		switch(this.props.granularity) {
		case DAILY:
			dates = d.getUsedDates();
			break;
		case MONTHLY:
			Set<Integer> months = d.getUsedMonths();
			c.clear();
			for(int year : years) {
				for(int month : months) {
					c.set(year, month-1, 1);
					dates.add(c.getTime());
					c.clear();
				}
			}
			break;
		case YEARLY:
			c.clear();
			for(int year : years) {
				c.set(year, 0, 1);
				dates.add(c.getTime());
				c.clear();
			}
			break;
		default:
			dates = d.getUsedDates();
		}
		return dates;
	}

	private String constructDateString(Date date) {
		return this.sdf.format(date);
	}

	
}
