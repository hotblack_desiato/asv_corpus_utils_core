/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.uni_leipzig.informatik.asv.corpusUtils.corpus.model;

/**
 * Exception that is thrown when the {@link Dictionary} is queried for a {@link WordType} with an ID
 * that can not be found in the {@link Dictionary}
 * @author Patrick Jaehnichen
 *
 */
public class WordTypeNotFoundException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5339612812125492001L;

	private WordToken wt;
	
	private Document d;
	
	public WordTypeNotFoundException(WordToken wt, Document d) {
		this.wt = wt;
		this.d = d;
	}

	@Override
	public String getMessage() {
		return "The word type with id=" + wt.getTypeId() + " could not be found, referenced by word token with id=" + wt.getIndexInDocument() + " in document " + d.getDescription();
	}
	
	
	
}
