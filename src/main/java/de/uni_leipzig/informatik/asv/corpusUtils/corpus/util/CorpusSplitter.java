/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.uni_leipzig.informatik.asv.corpusUtils.corpus.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Random;
import java.util.Set;

import de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.Corpus;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.DiachronicCorpus;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.Dictionary;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.Document;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.WordType;

/**
 * Convenience class to create training, test and validation corpora from a base {@link Corpus} object.
 * @author <a href="mailto:jaehnichen@informatik.uni-leipzig.de">Patrick Jaehnichen</a>
 *
 */
public class CorpusSplitter {
	
	
	/**
	 * Creates a training and validation corpus from a base corpus. The documents from the base corpus are shuffled
	 * and then distributed between training and validation set according to the provided ratio.
	 * @param source the underlying {@link Corpus}
	 * @param trainingSet an (empty) {@link Corpus} to which the training documents are added
	 * @param validationSet an (empty) {@link Corpus} to which the validation documents are added
	 * @param validationSetRatio the ratio, i.e. if set to .2, 20% of the documents go into the validation set
	 */
	public static void splitCorpus(Corpus source, Corpus trainingSet, Corpus validationSet, double validationSetRatio) {
		trainingSet.setDictionary(source.getDictionary().clone());
		validationSet.setDictionary(source.getDictionary().clone());
		boolean isDiachronic = source instanceof DiachronicCorpus;
		Date pubDate = null;
		HashMap<Integer, Boolean> excludeFromTraining = getTrainingExcludedDocuments(source, validationSetRatio, isDiachronic);
		HashMap<WordType, Integer> trainingContent = new HashMap<>(), validationContent = new HashMap<>();
		for(Document doc : source.getDocuments()) {
			if(isDiachronic) {
				pubDate = doc.getPublicationDate();
			}
			Boolean exclude = excludeFromTraining.get(doc.getId());
			if((exclude != null) && exclude) {
				addToCorpus(validationSet, doc);
				continue;
			}
			addToCorpus(trainingSet, doc);
		}
	
	}

	private static void addToCorpus(Corpus target, Document doc) {
		HashMap<WordType, Integer> content = new HashMap<>();
		int[] types = doc.getTypeList();
		int[] freqs = doc.getFrequencyList();
		for(int i=0;i<types.length;i++) {
			WordType wt = target.getWordType(types[i]);
			content.put(wt, freqs[i]);
		}
		target.createDocumentAndAdd(content, doc.getDescription(), doc.getPublicationDate());
	}
	
	/**
	 * Creates training, test and validation corpora from a base {@link Corpus}. Documents are shuffled, according to validationSetRatio,
	 * some of them go into the validation set. The rest of the documents is individually split and added to the training and test set as follows.
	 * For each document:
	 * <li>determine number of tokens to use for training according to trainingSetRatio, i.e. if this is set to .8, 80% of the tokens in a document go into the training set</li>
	 * <li>the rest of the tokens (at least 1) goes into the test set, this can now be used as a container of held-out second document parts in document completion</li>
	 * @param source the underlying data
	 * @param trainingSet an (empty) {@link Corpus} to which the training documents are added
	 * @param testSet an (empty) {@link Corpus} to which the test documents are added
	 * @param validationSet an (empty) {@link Corpus} to which the validation documents are added
	 * @param trainingSetRatio ratio of document content to place in the training set
	 * @param validationSetRatio ratio of documents to place in the validation set
	 */
	public static void splitCorpus(Corpus source, Corpus trainingSet, Corpus testSet, Corpus validationSet, double trainingSetRatio, double validationSetRatio) {
		trainingSet.setDictionary(source.getDictionary().clone());
		testSet.setDictionary(source.getDictionary().clone());
		validationSet.setDictionary(source.getDictionary().clone());
		
		double docLength, trainingLength, testLength, rnd;
		HashMap<WordType, Integer> trainingContent = new HashMap<>();
		HashMap<WordType, Integer> testContent = new HashMap<>();
		int sum, w;
		int[] freqList, typeList;
		Random rng = new Random();
		
		boolean isDiachronic = source instanceof DiachronicCorpus;
		Date pubDate = null;
		
		HashMap<Integer, Boolean> excludeFromTraining = getTrainingExcludedDocuments(source, validationSetRatio, isDiachronic);
		
		for(Document doc : source.getDocuments()) {
			if(isDiachronic) {
				pubDate = doc.getPublicationDate();
			}
			Boolean exclude = excludeFromTraining.get(doc.getId());
			if((exclude != null) && exclude) {
				addToCorpus(validationSet, doc);
				continue;
			}
			
			trainingContent.clear();
			testContent.clear();
			docLength = doc.getLength();
			trainingLength = Math.floor(docLength*trainingSetRatio);
			testLength = docLength - trainingLength;
			typeList = doc.getTypeList();
			freqList = doc.getFrequencyList();
			
			for(int i=0;i<testLength;++i) {
				//sample randomly from content
				rnd = rng.nextDouble() * docLength;
				sum = 0;
				for(w=0;w<freqList.length;++w) {
					sum += freqList[w];
					if(sum > rnd)
						break;
				}
				WordType wt = testSet.getWordType(typeList[w]);
				//increment the test content
				Integer oldCount = testContent.get(wt);
				if(oldCount != null)
					testContent.put(wt, oldCount+1);
				else
					testContent.put(wt, 1);
			}
			for(int i=0;i<typeList.length;++i) {
				WordType wtTest = testSet.getWordType(typeList[i]);
				WordType wtTrain = trainingSet.getWordType(typeList[i]);
				Integer testCount = testContent.get(wtTest);
				if(testCount == null)
					trainingContent.put(wtTrain, freqList[i]);
				else
					trainingContent.put(wtTest, freqList[i]-testCount);

				if(trainingContent.get(wtTest) < 1)
					trainingContent.remove(wtTrain);
			}
			Document trainingDoc = trainingSet.createDocumentAndAdd(trainingContent, doc.getDescription(), pubDate);
			Document testDoc = testSet.createDocumentAndAdd(testContent, doc.getDescription(), pubDate);
			//secondary ID of the testDocs is ID of trainingDocs, also store this in trainingDoc's secondary ID, used for perplexity computations
			testDoc.setSecondaryId(trainingDoc.getId());
		}
		
	}

	private static HashMap<Integer,Boolean> getTrainingExcludedDocuments(Corpus source, double validationSetRatio, boolean isDiachronic) {
		HashMap<Integer, Boolean> excludeFromTraining = new HashMap<>();
		if(isDiachronic) {
			DiachronicCorpus dSource = (DiachronicCorpus) source; 
			for(Date d : dSource.getUsedDates()) {
				Set<Integer> docIds = dSource.getDocumentsForDate(d);
				ArrayList<Integer> docIdList = new ArrayList<>(docIds);
				Collections.shuffle(docIdList);
				int lastValidationDoc = (int) Math.floor(docIdList.size()*validationSetRatio);
				for(int i=0;i<lastValidationDoc;i++)
					excludeFromTraining.put(docIdList.get(i), true);
			}
		} else {
			ArrayList<Document> docList = new ArrayList<>(source.getDocuments());
			Collections.shuffle(docList);
			int lastValidationDoc = (int) Math.floor(docList.size()*validationSetRatio);
			for(int i=0;i<lastValidationDoc;i++)
				excludeFromTraining.put(docList.get(i).getId(), true);
		}
		return excludeFromTraining;
	}
	
	
	/**
	 * Shuffles the source corpus and provides a new Corpus containing only numSamples randomly
	 * collected from src. The {@link Dictionary} in the resulting {@link Corpus} is pruned, so that it does not
	 * contain types not used in the sample. However, {@link WordType} IDs stay consistent.
	 * @param src the underlying data
	 * @param numSamples number of samples to collect
	 * @return the subsample as a {@link Corpus} object
	 */
	public static Corpus collectRandomSample(Corpus src, int numSamples) {
		if(src.getNumberOfDocuments() <= numSamples)
			return src;
		Corpus ret = new Corpus();
		//copy the dictionary
		ret.setDictionary(src.getDictionary().clone());
		Iterator<Document> srcDocIt = src.getShuffledDocumentIterator();
		for(int docCnt = 0;docCnt < numSamples;docCnt++) {
			Document d = srcDocIt.next();
			d.clone(ret);
		}
		//remove words not used in the sample
		ret.pruneDictionary();
		return ret;
	}

}
