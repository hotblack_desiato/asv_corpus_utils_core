/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
/**
 * 
 */
package de.uni_leipzig.informatik.asv.corpusUtils.corpus.importer;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.Corpus;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.util.ImportSettings;

/**
 * CSV Importer for AIF ATMT project. The first column represents a document ID, the second is the document content.
 * 
 * @author <a href="mailto:jaehnichen@informatik.uni-leipzig.de">Patrick Jaehnichen</a>
 *
 */
public class AIFCSVImporter extends TextImporter {
	/**
	 * Constructor.
	 * @param settings - the {@link ImportSettings}
	 */
	public AIFCSVImporter(ImportSettings settings) {
		super(settings);
		// TODO Auto-generated constructor stub
	}

	/* (non-Javadoc)
	 * @see de.uni_leipzig.informatik.asv.corpusUtils.corpus.importer.CorpusImporter#doImport(de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.Corpus)
	 */
	@Override
	public void doImport(
			Corpus c)
			throws IOException {
		String entry = "";
		BufferedReader br = new BufferedReader(new FileReader(this.props.dataFile));
		int count = 0;
		StringBuilder content = new StringBuilder();
		while((entry = br.readLine()) != null) {
			count++;
			String[] parts = entry.split("\t");
			content.append(parts[1]);
			if(count%10==0) {
				createDocumentFromStringAndAddToCorpus(c, content.toString(), "document " + parts[0], true);
				content.setLength(0);
			}
			if(count%1000==0)
				logger.info("done with " + count + " documents...");
		}
		br.close();
	}

}
