/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.uni_leipzig.informatik.asv.corpusUtils.corpus.importer;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

import de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.Corpus;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.WordType;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.state.DocumentState;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.state.WordTokenState;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.util.ImportSettings;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.utils.FileUtils;

public class TEVectorFileImporter extends CorpusImporter{
	
	public TEVectorFileImporter(ImportSettings settings) {
		super(settings);
	}
	/**
	 * Imports a corpus from a directory of vector files found by terminology extraction
	 * format per file is:
	 * term \t significance of term
	 * ...
	 * 
	 * @param c the corpus object in which to import the data
	 * @throws IOException if an exception occurred in reading the directory
	 */
	public void doImport(Corpus c) throws IOException {
		String filename = props.dataDir;
		File dir = new File(filename);
		if(!dir.isDirectory()) {
			System.out.println(filename + " is not a directory");
			System.exit(-1);
		}
		HashMap<WordType, Integer> docContent = new HashMap<WordType, Integer>();
		File[] fileList = collectFiles(filename);
		for(File f : fileList) {
			if(f.isDirectory()) continue;
			for(String line : FileUtils.readLinesFromFile(filename)) {
				String[] parts = line.split("\t");
				if(parts[0].split("\\s").length != 1) continue;
				String term = parts[0];
				Float sig = Float.valueOf(parts[1]);
				docContent.put(c.getWordType(term, true), Math.round(sig));
			}
			c.createDocumentAndAdd(docContent, f.getName());
			docContent.clear();
		}
			
	}

}
