/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.uni_leipzig.informatik.asv.corpusUtils.corpus.importer.database;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;

import de.uni_leipzig.informatik.asv.corpusUtils.corpus.importer.TextImporter;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.Corpus;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.Document;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.state.DocumentState;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.state.WordTokenState;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.util.DatabaseConnectionProvider;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.util.ImportSettings;

public class ElasticsearchCorpusImporter extends TextImporter {

	
	private static final Logger logger = LogManager.getLogger();
	
	public ElasticsearchCorpusImporter(ImportSettings settings) {
		super(settings);
	}

	@Override
	public void doImport(Corpus c) throws IOException {
		logger.debug("corpus is of type: " + c.getClass().getName());
		if(props.vocabFile != null)
			c.getDictionary().readDictionaryFile(props.vocabFile);
		System.out.println("using external vocabulary file: " + props.vocabFile);
		//fetch relevant documents
		Set<Date> usedDates = new HashSet<>();
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.ENGLISH);
		try {
			Client client = DatabaseConnectionProvider.getElasticsearchClient(props);

			SearchRequestBuilder srb = client.prepareSearch(props.index).setTypes(props.type);
			if(!props.query.trim().equals("")) {
				srb.setQuery(QueryBuilders.queryStringQuery(props.query));
			}
			SearchResponse resp = srb.setSize(1).get();
			srb.setSize(1000);
			
			int processed = 0;
			long all = resp.getHits().getTotalHits();
			logger.info("found " + all + " seach results, start import...");
			while(processed < all) {
				resp = srb.setFrom(processed).get();

				Iterator<SearchHit> iter = resp.getHits().iterator();
				while(iter.hasNext()) {
					SearchHit hit = iter.next();
					if(processed %1000 == 0)
						logger.info("processed " + processed + " documents so far");
					
					Map<String, Object> contents = hit.getSource();
					String pubDateString = (String)contents.get("publicationDate");
					if(pubDateString == null) {
						logger.info("skipping document, got null publication date string object");
						continue;
					}
					Date pubDate = df.parse(pubDateString);
					
					usedDates.add(pubDate);
					String description = (String)contents.get("headline");
					String content = (String)contents.get("content");
					
					if(!content.trim().equals("")) { 
						Document doc = createDocumentFromStringAndAddToCorpus(c, content, description, props.vocabFile == null, pubDate);
						doc.setExternalId(String.valueOf((Integer)contents.get("id")));
					}

					processed++;
				}			
			}			
		} catch(Exception e) {
			System.err.println(e.getMessage());
			e.printStackTrace();
		} finally {
			DatabaseConnectionProvider.closeElasticsearchClient();
		}
		logger.debug("found " + usedDates.size() + " different dates");
		logger.info("done with import");
	}
}
