/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.uni_leipzig.informatik.asv.corpusUtils.corpus.exporter;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Collection;

import de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.Corpus;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.Dictionary;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.WordType;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.util.ExportSettings;


/**
 * Exports a vocabulary, i.e. all {@link WordType} in a {@link Corpus}' {@link Dictionary}.
 * Line number corresponds to type id. Additionally, a second file is created with corpus frequencies of 
 * individual types.
 * @author <a href="mailto:jaehnichen@informatik.uni-leipzig.de">Patrick Jaehnichen</a>
 *
 */
public class VocabularyExporter extends CorpusExporter {
	/**
	 * Constructor with {@link ExportSettings}.
	 * @param settings - the export settings
	 */
	public VocabularyExporter(ExportSettings settings) {
		super(settings);
	}
	
	/**
	 * Exports a {@link Corpus}' {@link Dictionary} as described in the class' description.
	 * The output goes into the directory as specified by OUTPUT_DIR in {@link ExportSettings}.
	 * The target file is named as in the VOCAB_FILENAME, the frequency file defaults to VOCAB_FILENAME.freqs.
	 */
	@Override
	public void doExport(Corpus c)
			throws IOException {
		Collection<WordType> allWords = c.getAllWordTypes();
		BufferedWriter bw = new BufferedWriter(new FileWriter(this.props.outputDir + File.separator + this.props.vocabFilename));
		BufferedWriter bwFreq = new BufferedWriter(new FileWriter(this.props.outputDir + File.separator + this.props.vocabFilename + ".freqs"));
		int freq;
		String val;
		for(WordType wt : allWords) {
			freq = wt.getCorpusFrequency();
			val = wt.getValue();
			if(freq >= this.props.minFreq && freq <= this.props.maxFreq && val.length() >= this.props.minLength) {
				bw.write(val + "\n");
				bwFreq.write(freq + "\n");
			}
		}
		bw.close();
		bwFreq.close();
		
		
	}

}
