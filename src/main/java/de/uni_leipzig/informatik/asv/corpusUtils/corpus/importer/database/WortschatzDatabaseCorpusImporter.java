/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.uni_leipzig.informatik.asv.corpusUtils.corpus.importer.database;

import java.io.IOException;
import java.net.ConnectException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import de.uni_leipzig.informatik.asv.corpusUtils.corpus.importer.TextImporter;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.Corpus;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.util.DatabaseConnectionProvider;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.util.ImportSettings;

/**
 * Corpus importer for LU ASV Wortschatz data database format.
 * @author <a href="mailto:jaehnichen@informatik.uni-leipzig.de">Patrick Jaehnichen</a>
 *
 */
public class WortschatzDatabaseCorpusImporter extends TextImporter {
	
	//table names
	private static final String documentTable = "sources";
	private static final String sentenceTable = "sentences";
	private static final String sentenceToDocumentTable = "inv_so";
	
	//field names
	private static final String sourceId = "so_id";
	private static final String sentenceId = "s_id";
	private static final String sentence = "sentence";
	
	private Connection conn = null;

	public WortschatzDatabaseCorpusImporter(ImportSettings settings) {
		super(settings);
		try {
			conn = DatabaseConnectionProvider.getMysqlConnection(settings);
		} catch (ConnectException e) {
			e.printStackTrace();
			System.exit(-1);
		}
	}

	@Override
	public void doImport(Corpus c) throws IOException {
		//load dictionary
		c.getDictionary().readDictionaryFromDatabase(props);
		List<DocumentVO> docs = getDocumentIds();
		System.out.print("importing documents... ");
		for(DocumentVO docVO : docs) {
			String docContent = readDocumentContent(docVO);
			createDocumentFromStringAndAddToCorpus(c, docContent, docVO.getSource(), false, docVO.getDate());
		}
		System.out.print("done\n");
	}

	private String readDocumentContent(DocumentVO docVO) {
		//read text value of document
		String sql = "select " + sentence 
				+ " from " + sentenceTable + ", " + sentenceToDocumentTable 
				+ " where " + sentenceToDocumentTable + "." + sentenceId + " = " + sentenceTable + "." + sentenceId 
				+ " and " + sentenceToDocumentTable+"." + sourceId + " = " + docVO.getId();
		StringBuffer sb = new StringBuffer();
		try {
			PreparedStatement ps = conn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				sb.append(rs.getString(1));
			}
			rs.close();
			ps.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return sb.toString();
	}
	

	private List<DocumentVO> getDocumentIds() {
		List<DocumentVO> docList = new ArrayList<DocumentVO>();
		try {
			String sql = "select * from " + documentTable;
			System.out.print("fetching document ids...");
			PreparedStatement ps = conn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				docList.add(new DocumentVO(rs.getInt(1), rs.getString(2), rs.getDate(3)));
			}
			rs.close();
			ps.close();
			System.out.print(" done, got " + docList.size() + " document ids\n");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return docList;
	}

}
