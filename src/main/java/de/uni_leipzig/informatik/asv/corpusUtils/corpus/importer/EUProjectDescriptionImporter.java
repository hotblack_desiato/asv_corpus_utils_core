/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.uni_leipzig.informatik.asv.corpusUtils.corpus.importer;

import java.io.IOException;

import de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.Corpus;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.state.DocumentState;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.state.WordTokenState;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.util.ImportSettings;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.utils.FileUtils;

public class EUProjectDescriptionImporter extends TextImporter {

	public EUProjectDescriptionImporter(ImportSettings settings) {
		super(settings);
	}

	@Override
	public void doImport(Corpus c) throws IOException {
		String filename = props.dataFile;
		String[] lines = FileUtils.readLinesFromFile(filename);
		int ct = 0;
		int len = 0;
		String[] parts;
		for(String line : lines) {
			//remove masked double quotes
			line = line.replaceAll("\"\"", "");
			if(ct == 0) {
				parts = line.split(";");
				len = parts.length;
			} else {
				parts = new String[len];
				int partCt = 0;
				int nextSemicolon = line.indexOf(";");
				while(nextSemicolon != -1) {
					int nextQuotationMark = line.indexOf("\"");
					if(nextQuotationMark != -1 && nextQuotationMark < nextSemicolon) {
						//ignore this semicolon, it is masked
						int endingQuotationMark = line.indexOf("\"", nextQuotationMark+1);
						parts[partCt] = line.substring(nextQuotationMark+1, endingQuotationMark);
						if(endingQuotationMark <= line.length()-2) 
							line = line.substring(endingQuotationMark+2);
						else if(endingQuotationMark <= line.length()-1)
							line = line.substring(endingQuotationMark+1);
					} else {
						parts[partCt] = line.substring(0, nextSemicolon);
						line = line.substring(nextSemicolon+1);
					}
					nextSemicolon = line.indexOf(";");
					partCt++;
				}
				if(partCt == len-1)
					parts[partCt] = line;
			}
			
			if(ct!=0)
				createDocumentFromStringAndAddToCorpus(c, parts[parts.length-1], parts[1], true);
			
			ct++;
		}
		
	}

}
