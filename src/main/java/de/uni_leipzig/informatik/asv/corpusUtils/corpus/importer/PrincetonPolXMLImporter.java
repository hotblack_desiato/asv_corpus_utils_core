/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.uni_leipzig.informatik.asv.corpusUtils.corpus.importer;

import java.io.IOException;

import de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.Corpus;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.state.DocumentState;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.state.WordTokenState;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.util.ImportSettings;

/**
 * Importer class for XML files provided by Princeton political sciences.
 * (Liechtenstein Institute for self-determination)
 * @author Patrick Jaehnichen
 *
 */
public class PrincetonPolXMLImporter extends TextImporter {

	
	PrincetonPolXMLParser p;
	
	public PrincetonPolXMLImporter(ImportSettings settings) {
		super(settings);
	}

	@Override
	/**
	 * Import the XML file.
	 */
	public void doImport(Corpus c) throws IOException {
		p = new PrincetonPolXMLParser(this, c);
		p.parseDocument();
	}

}
