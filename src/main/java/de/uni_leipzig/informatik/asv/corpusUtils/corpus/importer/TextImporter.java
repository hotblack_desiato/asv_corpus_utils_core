/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.uni_leipzig.informatik.asv.corpusUtils.corpus.importer;

import java.io.UnsupportedEncodingException;
import java.text.Normalizer;
import java.text.Normalizer.Form;
import java.util.Date;
import java.util.HashMap;

import org.apache.commons.lang3.text.WordUtils;
import org.tartarus.snowball.SnowballProgram;

import de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.Corpus;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.Document;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.WordType;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.util.ContentCleaner;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.util.ImportSettings;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.util.Language;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.util.Stopwords;

/**
 * Base class for classes providing text import.
 * Documents are created from a content string after some preprocessing steps as white space truncation, stop word deletion, normalisation.
 * @author Patrick Jaehnichen <jaehnichen@informatik.uni-leipzig.de>
 *
 */
public abstract class TextImporter extends CorpusImporter {
	
	private SnowballProgram stemmer = null;

	protected TextImporter(ImportSettings settings) {
		super(settings);
		if(settings.useStemmer) {
			if(settings.languages.length > 1)
				logger.warn("Stemming only supported for a single language, there are " + settings.languages.length + " languages defined.");
			else {
				String languageName = WordUtils.capitalizeFully(Language.getLanguageName(settings.languages[0]));
				try {
					Class stemClass = Class.forName("org.tartarus.snowball.ext." + languageName + "Stemmer");
					stemmer = (SnowballProgram) stemClass.newInstance();
				} catch (ClassNotFoundException e) {
					logger.fatal("Stemmer for language " + languageName + " not supported.");
					e.printStackTrace();
				} catch (InstantiationException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				}
			}
		}
			
	}
	
	/**
	 * Creates a document from a string and adds it to the corpus.
	 * @param c - the corpus in which this document is located
	 * @param docContent - the content of the document
	 * @param description - the name of the source file (if any)
	 * @param expandDictionary - indicator, whether the dictionary is allowed to grow with unknown terms
	 * @param date - the timestamp of the document (if available)
	 * @return the newly created document
	 * @throws UnsupportedEncodingException 
	 */
	protected Document createDocumentFromStringAndAddToCorpus(Corpus c, String docContent, String description, boolean expandDictionary, Date... d) throws UnsupportedEncodingException {
		//preprocessing
		if(props.preprocess) {
			docContent = docContent.toLowerCase();
			docContent = ContentCleaner.cleanContent(docContent);
			docContent = ContentCleaner.cleanWhiteSpaces(docContent);
		}
		if(props.normalizeCurrency)
			docContent = ContentCleaner.normalizeCurrencyReferences(docContent, props.removeCurrency);
		if(props.normalizeNumbers)
			docContent = ContentCleaner.normalizeNumberOccurrences(docContent, props.removeNumbers);
		if(props.removePunctuation) 
			docContent = ContentCleaner.removePunctuation(docContent);
		if(props.removeDashes)
			docContent = ContentCleaner.removeDashes(docContent);
		String[] words = docContent.split("\\s+");
		HashMap<WordType, Integer> docContentMap = new HashMap<WordType, Integer>();
		outer:for(String w : words) {
			w = w.trim();
			//make sure only alphanum characters are left
			w = ContentCleaner.cleanWord(w);
			//if still empty words exist:
			if(w.equals(""))
				continue;
			//normalization
			if(props.normalizeCharacters)
				w = Normalizer.normalize(w, Form.NFKD);
			//stopword deletion
			if(props.removeStopwords) {
				if(props.languages.length == 1 && props.languages[0].equals("all"))
					if(Stopwords.isGenericStopWord(w))
						continue outer;
				for(String l : props.languages) {
					if(Stopwords.isStopWord(l, w)) {
						continue outer;
					}
				}
			}
			if(props.useStemmer && stemmer != null) {
				//use the snowball stemmer
				stemmer.setCurrent(w);
				stemmer.stem();
				w = stemmer.getCurrent();
			}
			
			WordType word = c.getWordType(w, expandDictionary);
			if(word == null)
				continue;
			Integer oldFreq = docContentMap.get(word);
			if(oldFreq == null)
				docContentMap.put(word, 1);
			else
				docContentMap.put(word, oldFreq+1);
		}
		return c.createDocumentAndAdd(docContentMap, description, d);
	}

	
}
