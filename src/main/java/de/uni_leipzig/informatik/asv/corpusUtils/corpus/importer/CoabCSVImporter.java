/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
/**
 * 
 */
package de.uni_leipzig.informatik.asv.corpusUtils.corpus.importer;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.List;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.Corpus;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.Document;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.util.ImportSettings;

/**
 * CSV Importer for COAB project. First column represents a document ID, second columns is a page ID (a second document specific ID),
 * the third column is the document content.
 * @author <a href="mailto:jaehnichen@informatik.uni-leipzig.de">Patrick Jaehnichen</a>
 *
 */
public class CoabCSVImporter extends TextImporter {

	
	/**
	 * Constructor.
	 * @param settings - the {@link ImportSettings}
	 */
	public CoabCSVImporter(ImportSettings settings) {
		super(settings);
	}

	/* (non-Javadoc)
	 * @see de.uni_leipzig.informatik.asv.corpusUtils.corpus.importer.CorpusImporter#doImport(de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.Corpus)
	 */
	@Override
	public void doImport(
			Corpus c)
			throws IOException {
		try {
			Reader in = new FileReader(this.props.dataFile);
			CSVParser parser = CSVFormat.TDF.withQuote(null).parse(in);
			List<CSVRecord> records = parser.getRecords();
			int allRecords = records.size();
			int processedRecords = 0;
			for (CSVRecord record : records) {
				processedRecords++;
				String docID = record.get(0);
				String pageID = record.get(1);
				String content = record.get(2);
				
				Document d = createDocumentFromStringAndAddToCorpus(c, content, pageID, true);
				d.setExternalId(docID);
				if(processedRecords%1000 == 0)
					System.out.println("done with " + processedRecords + " of " + allRecords);

			}
			parser.close();
			in.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
