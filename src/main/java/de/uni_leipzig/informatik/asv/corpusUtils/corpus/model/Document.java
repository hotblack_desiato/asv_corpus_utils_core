/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.uni_leipzig.informatik.asv.corpusUtils.corpus.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.TreeSet;

import cern.colt.map.OpenIntIntHashMap;

/**
 * The document class holds information about single documents in a corpus
 * @author Patrick Jaehnichen <jaehnichen@informatik.uni-leipzig.de>
 *
 */
public class Document implements Serializable{
	/**
	 * serial version uid
	 */
	private static final long serialVersionUID = -6828031254386609560L;
	/**
	 * the list of {@link WordToken}s in this document
	 */
	private ArrayList<WordToken> wordlist = new ArrayList<>();
	/**
	 * a list of {@link WordType} frequencies in this document
	 */
	private OpenIntIntHashMap wordFreqs = new OpenIntIntHashMap();
	/**
	 * a list of type ids
	 */
	private int[] typeList;
	/**
	 * a list of word frequencies, corresponds to typeList
	 */
	private int[] freqList;
	/**
	 * the corpus to which this document belongs
	 */
	private Corpus corpus;
	/**
	 * the position of the document in the corpus, mainly stored to ease bookkeeping
	 */
	private int id = 0;
	/**
	 * a description of the document
	 */
	private String desription;
	/**
	 * the state of the document
	 */
	private Object state;
	
	/**
	 * an external id from another source
	 */
	private String externalId;
	
	private int secondaryId = -1;
	
	/**
	 * a bucket for minHash LSH's for later deduplication and nearest neighbor calculations
	 */
	private int[] hashBuckets;
	
	private boolean[] binarySparseVector;
	private TreeSet<Integer> sortedTypeList;
	protected Date publicationDate;
	
	public int[] getHashBuckets() {
		return this.hashBuckets;
	}

	public void setHashBuckets(int[] hashBuckets) {
		this.hashBuckets = hashBuckets;
	}

	/**
	 * Constructs an empty document object given the corpus and source file.
	 * This is the preferred method to create documents and add them to a corpus. 
	 * @param c the corpus
	 * @param meta the meta data
	 */
	public Document(Corpus c, String fileSource, Date... pubDate) {
		this.corpus = c;
		this.desription = fileSource;
		if(pubDate != null && pubDate.length > 0)
			this.publicationDate = pubDate[0];
		this.corpus.addDocument(this);
	}
	
	void setId(int id) {
		this.id = id;
		//initially set secondary id to this too
		this.secondaryId = id;
	}
	
	/**
	 * @return the externalId
	 */
	public String getExternalId() {
		return this.externalId;
	}

	/**
	 * @param externalId the externalId to set
	 */
	public void setExternalId(String externalId) {
		this.externalId = externalId;
	}

	/**
	 * @return the secondaryId
	 */
	public int getSecondaryId() {
		return this.secondaryId;
	}

	/**
	 * @param secondaryId the secondaryId to set
	 */
	public void setSecondaryId(int secondaryId) {
		this.secondaryId = secondaryId;
	}

	/**
	 * Getter for the index position in the parent corpus.
	 * Basically, order is insignificant but this having a stable index position
	 * eases bookkeeping in many applications.
	 * @return the index in the corpus
	 */
	public int getId() {
		return this.id;
	}
	
	void addWord(WordType type, int freq) throws WordTypeNotFoundException {
		for(int i=1;i<= freq; i++) {
			addWord(new WordToken(type, this.wordlist.size()));
		}
	}
	
	/**
	 * Getter for the number of {@link WordType}s in the document.
	 * @return the number of {@link WordType}s
	 */
	public int getNumberOfWordTypes() {
		return this.wordFreqs.size();
	}
	
	/**
	 * Gets the type list as an integer array. Helps to speed up processes that often need to look at this.
	 * @return the type list
	 */
	public int[] getTypeList() {
		if(this.typeList == null)
			this.typeList = this.wordFreqs.keys().elements();
		return this.typeList;
	}
	
	/**
	 * Gets the frequency list as an integer array. Frequencies correspond to the types given by getTypeList() at the same position.
	 * @return the frequency list
	 */
	public int[] getFrequencyList() {
		if(this.freqList == null)
			this.freqList = this.wordFreqs.values().elements();
		return this.freqList;
	}
	
	/**
	 * Getter for a {@link WordType} frequency map. Keys in the map correspond to
	 * {@link WordType}-IDs, values are their frequencies in this document.
	 * @return the {@link WordType} frequency map
	 */
	public OpenIntIntHashMap getWordFrequencies() {
		return (OpenIntIntHashMap) this.wordFreqs.copy();
	}
	
	/**
	 * Adds the given {@link WordToken} to the document's content.
	 * @param token - the {@link WordToken} to add
	 * @throws WordTypeNotFoundException when the type of the token is unknown
	 */
	public void addWord(WordToken token) throws WordTypeNotFoundException {
		WordType wt = token.getType();
		if(wt == null)
			throw new WordTypeNotFoundException(token, this);
		this.wordFreqs.put(wt.getId(), this.wordFreqs.get(wt.getId()) +1);
		wt.increaseCorpusFrequency();
		wt.increaseDocumentFrequency(getId());
		this.wordlist.add(token);
		this.corpus.increaseCorpusSize();
	}
	
	/**
	 * Rebuilds the frequency list. This is automatically done after pruning the {@link Dictionary}.
	 * Use this method after making changes to the {@link Dictionary}
	 */
	public void rebuildFreqList(){
		
		Iterator<WordToken> i = this.wordlist.iterator();
		
		this.wordFreqs = new OpenIntIntHashMap();
		
		while(i.hasNext())
		{
			WordToken t = i.next();
			this.wordFreqs.put(t.getTypeId(), this.wordFreqs.get(t.getTypeId()) +1);
			
		}
		this.typeList = this.wordFreqs.keys().elements();
		this.freqList = this.wordFreqs.values().elements();
		
		}
	
	/**
	 * Removes all tokens that belong to one of the passed types. Used for removing high 
	 * or low frequency terms from the corpus.
	 * @param types - the {@link WordType}s to remove
	 */
	public int pruneTypes(HashSet<Integer> types) {
		
		//System.out.println(this.getId() + ": Document cleaned!");
		
		int deleted = 0;
		Iterator<WordToken> it = this.wordlist.iterator();
		
		while(it.hasNext())
		{
			WordToken wt = it.next();
			
			if(types.contains(wt.getTypeId()))
			{
				it.remove();
				deleted += 1;
			}
		}
		
		int[] keys = this.wordFreqs.keys().copy().elements();
		
		for(int key : keys)
		{
			if(types.contains(key))
				this.wordFreqs.removeKey(key);
		}
		
		return deleted;
	}
	
	/**
	 * Getter for document content.
	 * @return a list of {@link WordToken}s in this document.
	 */
	public ArrayList<WordToken> getWords() {
		return this.wordlist;
	}
	
	/**
	 * Clears the word list.
	 */
	public void clearWords() {
		for(WordToken token : this.wordlist) {
			WordType wt = this.corpus.getWordType(token.getTypeId());
			wt.decreaseCorpusFrequency();
			wt.decreaseDocumentFrequency(getId());
		}
		this.corpus.decreaseCorpusSize(this.wordlist.size());
		this.wordlist.clear();
	}

	/**
	 * Getter for document length.
	 * @return the number of {@link WordToken}s in this document.
	 */
	public int getLength() {
		return this.wordlist.size();
	}
	
	/**
	 * Creates a deep copy of this document.
	 * @param c - the corpus of the document copy
	 * @return a copy of the document
	 */
	public Document clone(Corpus c) {
		Document d = new Document(c, this.desription);
		d.wordlist = new ArrayList<>();
		for(WordToken wt : this.wordlist) {
			try {
				d.addWord(wt.clone());
			} catch (WordTypeNotFoundException e) {
				//this should not happen, however if it does, fail silently and skip token
			}
		}
		d.id = this.id;
		return d;
	}

	/**
	 * Getter for the description.
	 * @return the description for this document
	 */
	public String getDescription() {
		return this.desription;
	}

	/**
	 * Getter for document state.
	 * @return the state
	 */
	public <D> D getState() {
		return (D) this.state;
	}

	/**
	 * Setter for document state.
	 * @param state the state to set
	 */
	public void setState(Object state) {
		this.state = state;
	}

	public boolean[] getBinarySparseVector() {
		if(this.binarySparseVector != null)
			return this.binarySparseVector;
		this.binarySparseVector = new boolean[this.corpus.getNumberOfWordTypes()];
		
		for(int idx : getTypeList())
		{
			this.binarySparseVector[idx] = true;
		}
		
		return this.binarySparseVector;
	}
	
	public TreeSet<Integer> getSortedTypeList() {
		if(this.sortedTypeList != null)
			return this.sortedTypeList;
		this.sortedTypeList = new TreeSet<>();
		for(int type : getTypeList())
			this.sortedTypeList.add(type);
		return this.sortedTypeList;
	}

	/**
	 * Gets the timestamp of this document.
	 * @return the timestamp as {@link Date} object.
	 */
	public Date getPublicationDate() {
		return publicationDate;
	}

	/**
	 * Gets the date as number of days since epoch. Useful when exporting as a simple way to serialize {@link Date}s
	 * @return number of days since epoch
	 */
	public long getDateAsDaysAfterEpoch() {
		//1000 ms * 60s * 60min * 24h
		return publicationDate.getTime() / (1000 * 60 * 60 * 24);
	}
	
}
