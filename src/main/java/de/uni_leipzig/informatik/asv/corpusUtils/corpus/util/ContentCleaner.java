/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.uni_leipzig.informatik.asv.corpusUtils.corpus.util;

import java.text.Normalizer;
import java.util.Vector;
import java.util.regex.Pattern;

/**
 * Convenience class for content cleaning. Allows stripping of hyperlinks, punctuation removing,
 * currency reference normalisation and white space cleaning by now.
 * @author Patrick Jaehnichen <jaehnichen@informatik.uni-leipzig.de>
 *
 */
public class ContentCleaner {
	private static final Pattern multipleWhitespacePattern = Pattern.compile("\\s+");
	private static final Pattern lineBreakPattern = Pattern.compile("\n");
	private static final Pattern contentTagsWholePattern = Pattern.compile("<.*?>");
	private static final Pattern contentTagsStartPattern = Pattern.compile("^.*?>");
	private static final Pattern contentTagsEndPattern = Pattern.compile("<.*$");
	private static final Pattern scriptPattern = Pattern.compile("<script .*?>.*?</script>");
	private static final Pattern percentChars = Pattern.compile("%[a-zA-Z]");
	private static final Pattern noAlphaNumPattern = Pattern.compile("[»«]");
	private static final Pattern diacriticPattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");

	
	private static final Pattern currencyReferencePattern = Pattern.compile("\\s+((\\$|€)\\s?\\d+|\\d+\\s?(\\$|€))");
	private static final Pattern numberOccurrencePattern = Pattern.compile("\\d+(\\.|,)?\\d+");
	private static final Pattern generalPunctuationPattern = Pattern.compile("[\\p{Punct}&&[^-_]]");
	private static final Pattern dashPattern = Pattern.compile("\\s+[-—]\\s+");
	
	
	private static final String EMPTY_STRING = "";
	private static final String WHITESPACE = " "; 
	private static final String STRIP_OUT_LINK_PATTERN = "href=";
	private static final String CURRENCY_REFERENCE = " CURRENCY_REFERENCE ";
	private static final String NUMBER_OCCURRENCE = " NUMBER_OCCURRENCE ";
	
	/**
	 * Removes all html in a string and just leaves the raw text in between html tags.
	 * @param line - the line to clean
	 * @return the raw text between html tags
	 */
	public static String cleanContent(String line) {
		line = line.trim();
		//remove script tags with content
		line = scriptPattern.matcher(line).replaceAll(EMPTY_STRING);
		line = contentTagsWholePattern.matcher(line).replaceAll(EMPTY_STRING);
//		line = contentMarksPattern.matcher(line).replaceAll(WHITESPACE);
		//get partial html in front
		line = contentTagsStartPattern.matcher(line).replaceAll(EMPTY_STRING);
		//get partial html at end
		line = contentTagsEndPattern.matcher(line).replaceAll(EMPTY_STRING);
		//remove double whitespaces
		line = multipleWhitespacePattern.matcher(line).replaceAll(WHITESPACE);
		//remove strange characters
		line = percentChars.matcher(line).replaceAll(WHITESPACE);
		return line.trim();
	}
	
	/**
	 * Removes punctuation in the given string. Uses the Posix p{Punct} class for this.
	 * @param input - the input to be cleaned
	 * @return input without punctuation
	 */
	public static String removePunctuation(String input) {
		input = input.trim();
		String output = generalPunctuationPattern.matcher(input).replaceAll(EMPTY_STRING);
		return output.trim();
	}
	
	/**
	 * Removes dashes from the given input string and replaces it with a whitespace.
	 * @param input - the input string
	 * @return input with dashes replaced by a whitespace
	 */
	public static String removeDashes(String input) {
		input = input.trim();
		String output = dashPattern.matcher(input).replaceAll(WHITESPACE);
		return output.trim();
	}
	
	/**
	 * Normalizes any currency references in the input to the fixed string "CURRENCY_REFERENCE"
	 * @param input - the input to be checked
	 * @return the normalised input
	 */
	public static String normalizeCurrencyReferences(String input, boolean remove) {
		input = input.trim();
		String replacement = CURRENCY_REFERENCE;
		if(remove)
			replacement = EMPTY_STRING;
		String output = currencyReferencePattern.matcher(input).replaceAll(replacement);
		return output.trim();
	}
	
	/**
	 * Normalizes any number occurrence in the input to the fixed string "NUMBER_OCCURRENCE".
	 * @param input - the input to be checked
	 * @return the normalised output
	 */
	public static String normalizeNumberOccurrences(String input, boolean remove) {
		input = input.trim();
		String replacement = NUMBER_OCCURRENCE;
		if(remove)
			replacement = EMPTY_STRING;
		String output = numberOccurrencePattern.matcher(input).replaceAll(replacement);
		return output.trim();
	}
	
	/**
	 * Strips out all links in the given content string, keeps track of found hyperlinks and cleans content.
	 * The content is cleaned be the {@link cleanContent} method.
	 * @param content - the content to be checked
	 * @return a vector containing the cleaned content as the first element and all found hyperlinks as subsequent ones
	 */
	public static Vector<String> stripOutLinks(String content) {
		Vector<String> output = new Vector<String>();
		
		content = content.toLowerCase();
		int startPos = content.indexOf(STRIP_OUT_LINK_PATTERN);
		while(startPos > 0) {
			int startOfUrl = startPos + STRIP_OUT_LINK_PATTERN.length() + 1;
			if(startOfUrl > content.length())
				break;
			int endOfUrlQuote = content.indexOf('"', startOfUrl);
			int endOfUrlSingleQuote = content.indexOf('\'', startOfUrl);
			int endOfUrl;
			if(endOfUrlQuote < 0 && endOfUrlSingleQuote < 0) {
				endOfUrl = -1;
			} else if(endOfUrlQuote < 0) {
				endOfUrl = endOfUrlSingleQuote;
			} else if(endOfUrlSingleQuote < 0) {
				endOfUrl = endOfUrlQuote;
			} else {
				endOfUrl = endOfUrlSingleQuote < endOfUrlQuote ? endOfUrlSingleQuote : endOfUrlQuote;
			}
			//int endOfUrl = endOfUrlQuote;
			String url = "";
			if(endOfUrl > 0) {
				url = content.substring(startOfUrl, endOfUrl);
				if(!ignoreUrl(url)) {
					output.add(url);
				}
			} else {
				url = content.substring(startOfUrl);
				if(!ignoreUrl(url)) {
					output.add(url);
				}
				break;
			}
			startPos = content.indexOf(STRIP_OUT_LINK_PATTERN, endOfUrl);
		}
		content = cleanContent(content);
		output.add(0, content);
		return output;
	}
	
	private static boolean ignoreUrl(String url) {
		 return !(url.startsWith("http") && !(url.endsWith("jpg") || url.endsWith("jpeg") || url.endsWith("gif")));
	}
	
	/**
	 * Cleans whitespaces (i.e. reduces arbitrarily long sequences of whitespaces to just one whitespace char) and linebreaks.
	 * @param line - the line to be checked
	 * @return the cleaned string
	 */
	public static String cleanWhiteSpaces(String line) {
		line = line.trim();
		line = lineBreakPattern.matcher(line).replaceAll(WHITESPACE);
		
		line = multipleWhitespacePattern.matcher(line).replaceAll(WHITESPACE);
		
//		line = whitespaceStartPattern.matcher(line).replaceAll(EMPTY_STRING);
//		line = whitespaceEndPattern.matcher(line).replaceAll(EMPTY_STRING);
//		Matcher m = doubleWhitespacePattern.matcher(line);
//		while(m.find()) {
//			line = m.replaceAll(WHITESPACE);
//			m = doubleWhitespacePattern.matcher(line);
//		}
		return line;
	}
	/**
	 * Cleans the final word, i.e. removes any non-alphanumeric characters and normalizes individual characters.
	 * @param w - the raw word
	 * @return the cleaned word
	 */
	public static String cleanWord(String w) {
		w = w.trim();
		w = noAlphaNumPattern.matcher(w).replaceAll(EMPTY_STRING);
	    w = Normalizer.normalize(w, Normalizer.Form.NFC); 
//	    w = diacriticPattern.matcher(w).replaceAll("");
		return w.trim();
	}
	
}
