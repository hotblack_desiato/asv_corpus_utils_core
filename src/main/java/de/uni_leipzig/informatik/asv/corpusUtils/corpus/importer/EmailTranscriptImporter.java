/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.uni_leipzig.informatik.asv.corpusUtils.corpus.importer;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.Corpus;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.state.DocumentState;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.state.WordTokenState;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.util.ImportSettings;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.utils.FileUtils;

public class EmailTranscriptImporter extends TextImporter {

	public EmailTranscriptImporter(ImportSettings settings) {
		super(settings);
	}

	/**
	 * Imports a directory of email transcripts. Each file should contain exactly one email.
	 * All lines before the subject line are omitted as well as citations.
	 * @param c the corpus object in which to import the data
	 * @throws IOException if reading a file fails
	 */
	@Override
	public void doImport(
		Corpus c)
		throws IOException {
		String filename = props.dataDir;
		File f = new File(filename);
		if(!f.isDirectory())
			throw new IllegalArgumentException("Please provide a directory, " + filename + " is not a directory.");
		File[] files = collectFiles(filename);
		for(File file : files) {
			parseDoc(c, file);
		}
	}

	private void parseDoc(Corpus c, File file) throws IOException {
		String[] docStrings = FileUtils.readLinesFromFile(file.getAbsolutePath());
		StringBuilder sb = new StringBuilder();
		boolean contentStarted = false;
		int idx = -1;
		Date pubDate = null;
		for(String line : docStrings) {
			if((idx = line.indexOf("Sent: ")) > -1) {
				line.substring(idx+6);
				SimpleDateFormat sdf = new SimpleDateFormat("", Locale.ENGLISH);
				try {
					pubDate = sdf.parse(line);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if(!line.startsWith("Subject: ") && !contentStarted)
				continue;
			else
				contentStarted = true;
			if(contentStarted) {
				if(!line.startsWith(">")) {
					sb.append(" ").append(line);
				}
			}
		}
		
		
		createDocumentFromStringAndAddToCorpus(c, sb.toString(), file.getName(), true, pubDate);
	}

}
