/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.uni_leipzig.informatik.asv.corpusUtils.corpus.importer;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import de.uni_leipzig.informatik.asv.corpusUtils.corpus.importer.database.ElasticsearchCorpusImporter;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.importer.database.WortschatzDatabaseCorpusImporter;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.Corpus;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.DiachronicCorpus;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.WordTypeNotFoundException;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.util.ImportSettings;

/**
 * Base class for corpus importers. Classes that import a corpus should be derived from this class.
 * @author Patrick Jaehnichen <jaehnichen@informatik.uni-leipzig.de>
 *
 */
public abstract class CorpusImporter {
	
	protected ImportSettings props;
	
	protected static final Logger logger = LogManager.getLogger();
	
	protected CorpusImporter(ImportSettings settings) {
		this.props = settings;
	}
	
	/**
	 * Do the actual import.
	 * @param c - the corpus in which to import the data
	 * @throws IOException if there were problems in reading a file
	 * @throws WordTypeNotFoundException if a word type is not found in the dictionary during import and the dictionary is not allowed to grow with the data 
	 */
	public abstract void doImport(Corpus c) throws IOException;
	
	/**
	 * Collects all {@link File}s in a directory using a {@link FileFilter}
	 * @param dirname - the name of the directory.
	 * @param ff - the {@link FileFilter} to use
	 * @return an array of {@link File}s
	 */
	public static File[] collectFiles(String dirname, FileFilter ff) {
		File dir = new File(dirname);
		if(dir.isFile())
			return new File[]{dir};
		ArrayList<File> collectedFiles = new ArrayList<>();
		File[] dirEntries = dir.listFiles();
		for(File f : dirEntries) {
			if(f.isFile()) {
				if(ff == null || ff.accept(f)) {
					collectedFiles.add(f);
					continue;
				}
			} else
				collectedFiles.addAll(Arrays.asList(collectFiles(f.getAbsolutePath(), ff)));
		}
		return collectedFiles.toArray(new File[]{});
	}
	
	/**
	 * Creates a simple file tyep {@link FileFilter}.
	 * @param type - the file type to accept
	 * @return a {@link FileFilter} that accepts only files with extension type
	 */
	public static FileFilter createFileTypeFileFilter(final String type) {
		return new FileFilter() {
			@Override
			public boolean accept(File pathname) {
				return pathname.getName().endsWith(type);
			}
		};
	}

	/**
	 * Collects all {@link File}s in a directory
	 * @param dirname - the name of the directory.
	 * @return an array of {@link File}s
	 */
	public static File[] collectFiles(String dirname) {
		return collectFiles(dirname, null);
	}
	
	/**
	 * Collects all subdirectories of a directory.
	 * @param dirname - the name of the directory
	 * @return an array of {@link File}s
	 */
	public static File[] collectSubdirs(String dirname) {
		File dir = new File(dirname);
		if(dir.isFile())
			throw new IllegalArgumentException("Please provide a directory");
		ArrayList<File> collectedSubdirs = new ArrayList<File>();
		collectedSubdirs.add(dir);
		File[] dirEntries = dir.listFiles();
		for(File f : dirEntries) {
			if(f.isDirectory()) {
				collectedSubdirs.remove(dir);
				collectedSubdirs.addAll(Arrays.asList(collectSubdirs(f.getAbsolutePath())));
			}
		}
		
		return collectedSubdirs.toArray(new File[]{});
	}

	/**
	 * Convenience method for importing a feature frequency a.k.a. SVMLight a.k.a. c-lda formatted corpus from text files.
	 * For a more detailed description of the data format see {@link FeatureFrequencyImporter}
	 * @param settings - the file containing the import settings
	 * @throws IOException if there were problems in reading a file
	 */
	public static void importFeatureFrequencyCorpus(Corpus c, ImportSettings settings) throws IOException {
		CorpusImporter importer = new FeatureFrequencyImporter(settings);
		importer.doImport(c);
	}


	/**
	 * Convenience method for importing a feature frequency a.k.a. SVMLight a.k.a. c-lda formatted corpus from text files.
	 * For a more detailed description of the data format see {@link FeatureFrequencyImporter}
	 * This imports a whole directory of files.
	 * @param settings - the file containing the import settings
	 * @throws IOException if there were problems in reading a file
	 */
	public static void importFeatureFrequencyCorpusDir(Corpus c, ImportSettings settings) throws IOException {
		c.getDictionary().readDictionaryFile(settings.vocabFile);
		File[] files = collectFiles(settings.dataDir);
		CorpusImporter i = new FeatureFrequencyImporter(settings);
		for(File f : files) {
			settings.dataFile = f.getPath();
			i.doImport(c);
		}
		c.pruneDictionary();
	}
	
	/**
	 * Convenience method for importing a feature frequency a.k.a. SVMLight a.k.a. c-lda formatted corpus from text files.
	 * For a more detailed description of the data format see {@link FeatureFrequencyImporter}
	 * Time information is taken from the directory structure. The direct parent directory is assumed to be the day of publication,
	 * its parent the month, and the month's parent directory to represent the year of publication.
	 * @param settings - the file containing the import settings
	 * @throws IOException if there were problems in reading a file
	 */
	public static void importDiachronicFeatureFrequencyCorpus(DiachronicCorpus dc, ImportSettings settings) throws IOException {
		dc.getDictionary().readDictionaryFile(settings.vocabFile);
		System.out.println("read dictionary, size: " + dc.getNumberOfWordTypes());
		CorpusImporter importer = new DiachronicFeatureFrequencyImporter(settings);
		importer.doImport(dc);
		System.out.println("did import, corpus size: " + dc.getNumberOfDocuments() + " in " + dc.getNumberOfDates() + " time slices");
	}
	
	/**
	 * Convenience method for importing a plain text corpus from text files.
	 * @param settings - the file containing the import settings
	 * @throws IOException if there were problems reading a file
	 */
	public static void importPlainTextCorpus(Corpus c, ImportSettings settings) throws IOException {
		CorpusImporter importer = new PlainTextImporter(settings);
		importer.doImport(c);
	}


	/**
	 * Convenience method for importing a TE vector corpus from a text file.
	 * For a more detailed description of the data format see {@link TEVectorFileImporter}
	 * @param settings - the file containing the import settings
	 * @throws IOException if there were problems in reading a file
	 */
	public static void importTEVectorCorpus(Corpus c, ImportSettings settings) throws IOException {
		CorpusImporter importer = new TEVectorFileImporter(settings);
		importer.doImport(c);
	}
	
	/**
	 * Convenience method for importing the NYT corpus from either text files or the original gziped input files.
	 * For a more detailed description of the data format see {@link NYTDocumentTextImporter}
	 * @param settings - the file containing the import settings
	 * @throws IOException if there were problems in reading a file
	 */
	public static void importNYTCorpus(Corpus c, ImportSettings settings) throws IOException, ClassNotFoundException {
		CorpusImporter importer = new NYTDocumentTextImporter(settings);
		importer.doImport(c);
	}

	/**
	 * Convenience method for importing the NYT corpus from either text files or the original gziped input files.
	 * For a more detailed description of the data format see {@link NYTDocumentTextImporter}
	 * @param settings - the file containing the import settings
	 * @throws IOException if there were problems in reading a file
	 */
	public static void importNYTCorpus(DiachronicCorpus c, ImportSettings settings) throws IOException, ClassNotFoundException, WordTypeNotFoundException {
		CorpusImporter importer = new NYTDocumentTextImporter(settings);
		importer.doImport(c);
	}
	
	
	/**
	 * Convenience method for importing a corpus from the Wortschatz database.
	 * @param settings - the file containing the import settings
	 * @throws IOException if there were problems in reading a file
	 */
	public static void importWortSchatzCorpus(Corpus c, ImportSettings settings) throws IOException {
		WortschatzDatabaseCorpusImporter importer = new WortschatzDatabaseCorpusImporter(settings);
		importer.doImport(c);
	}

	/**
	 * Convenience method for importing a diachronic corpus from the Wortschatz database.
	 * @param settings - the file containing the import settings
	 * @throws IOException if there were problems in reading a file
	 */
	public static void importDiachronicWortSchatzCorpus(DiachronicCorpus c, ImportSettings settings) throws IOException {
		WortschatzDatabaseCorpusImporter importer = new WortschatzDatabaseCorpusImporter(settings);
		importer.doImport(c);
	}
	
	/**
	 * Convenience method for importing a corpus from the elasticsearch index.
	 * @param settings - the file containing the import settings
	 * @throws IOException if there were problems in reading a file
	 */
	public static void importElasticCorpus(Corpus c, ImportSettings settings) throws IOException {
		ElasticsearchCorpusImporter importer = new ElasticsearchCorpusImporter(settings);
		importer.doImport(c);
	}
	
	/**
	 * Convenience method for importing a diachronic corpus from the elasticsearch index.
	 * @param settings - the file containing the import settings
	 * @throws IOException if there were problems in reading a file
	 */
	public static void importElasticDiachronousCorpus(DiachronicCorpus c, ImportSettings settings) throws IOException {
		ElasticsearchCorpusImporter importer = new ElasticsearchCorpusImporter(settings);
		importer.doImport(c);
	}
	
	public static void importEmailTranscriptCorpus(Corpus c, ImportSettings settings) throws IOException {
		EmailTranscriptImporter imp = new EmailTranscriptImporter(settings);
		imp.doImport(c);
	}
	
}
