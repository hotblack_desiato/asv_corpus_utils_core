/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.uni_leipzig.informatik.asv.corpusUtils.corpus.util;

public enum DateFormatUtil {
	


	DATE_FORMAT_TWITTER("EEE MMM dd HH:mm:ss Z yyyy"),
	DATE_FORMAT_FACEBOOK("yyyy-MM-dd'T'HH:mm:ssZ"),
	DATE_FORMAT_WORTSCHATZ("yyyy-MM-dd"),
	DATE_FORMAT_LCM("yyyy-MM-dd'T'HH:mm:ss'Z'"),
	
		;
		
	private final String value;
	
	private DateFormatUtil(final String value){
		this.value = value;
	}
	
	public String getName(){
		return this.value;
	}



}
