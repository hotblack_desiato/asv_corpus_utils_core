/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.uni_leipzig.informatik.asv.corpusUtils.corpus.importer;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.Corpus;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.Document;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.util.DateFormatUtil;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.util.ImportSettings;

public class CSVDocIdDateTextImporter  extends TextImporter {

	private String inputDateFormat = DateFormatUtil.DATE_FORMAT_LCM.getName();

	
	public CSVDocIdDateTextImporter(ImportSettings settings) {
		super(settings);
		String dateFormatSetting = this.props.inputDateFormat;
		System.out.println("dateFormatSetting: '" + dateFormatSetting + "'");
		switch(dateFormatSetting) {
		case "LCM" :
			this.inputDateFormat = DateFormatUtil.DATE_FORMAT_LCM.getName();
			System.out.println("using date format LCM: '" + DateFormatUtil.DATE_FORMAT_LCM.getName() + "'");
			break;
		case "Twitter":
			this.inputDateFormat = DateFormatUtil.DATE_FORMAT_TWITTER.getName();
			System.out.println("using date format TWITTER" + DateFormatUtil.DATE_FORMAT_LCM.getName() + "'");
			break;
		case "Facebook":
			this.inputDateFormat = DateFormatUtil.DATE_FORMAT_FACEBOOK.getName();
			System.out.println("using date format FACEBOOK" + DateFormatUtil.DATE_FORMAT_LCM.getName() + "'");
			break;
		case "Wortschatz":
			this.inputDateFormat = DateFormatUtil.DATE_FORMAT_WORTSCHATZ.getName();
			System.out.println("using date format WORTSCHATZ" + DateFormatUtil.DATE_FORMAT_LCM.getName() + "'");
			break;
		default:
			this.inputDateFormat = dateFormatSetting;
			System.out.println("using date format DEFAULT:" + dateFormatSetting);

		}		
	}

	
	
	@Override
	public void doImport(Corpus c) throws IOException {
		try {
			Reader in = new FileReader(props.dataFile);
			CSVParser parser = CSVFormat.TDF.withQuote(null).parse(in);
			DateFormat df = new SimpleDateFormat(inputDateFormat, Locale.ENGLISH);
			
			List<CSVRecord> records = parser.getRecords();
			int allRecords = records.size();
			int processedRecords = 0;
			for (CSVRecord record : records) {
				processedRecords++;
				String docID = record.get(0);
				String dateString = record.get(1);
				String content = record.get(2);
				Date date = df.parse(dateString);
				Document d = createDocumentFromStringAndAddToCorpus(c, content, docID, true, date);
				d.setExternalId(docID);
				if(processedRecords%1000 == 0)
					System.out.println("done with " + processedRecords + " of " + allRecords);

			}
			in.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		
	}

}
