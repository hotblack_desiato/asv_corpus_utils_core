# README #

This package is intended to be used for Java based text analysis software. It contains data structures to represent textual data in a corpus comprised of documents. Additionally, there is some functionality to store preprocessed documents in an elastic search index to gain easy and quick access to the data. Different source data types have been considered already, see the Main class for more descriptions. The topic-model package heavily relies on this package.

### License ###
This package is licensed under GPLv3. If you care to use it in a context not covered by this license, feel free to contact us.

### Contact ###

[Patrick](mailto:patrick.jaehnichen@hu-berlin.de)